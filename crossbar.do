echo Compile vhd files
vcom src/crossbar/* src/tb/tb_crossbar20x20.vhd

echo starting tb simulation
vsim work.tb_crossbar20x20




echo adding wave signals
do tb_crossbar20x20_wave.do


echo running for 6000 ns
run 100 ns
wave zoom full
#wave zoom in 2.5
