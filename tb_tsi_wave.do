onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group tsi /tb_tsi/clk
add wave -noupdate -expand -group tsi /tb_tsi/reset
add wave -noupdate -expand -group tsi /tb_tsi/data_valid
add wave -noupdate -expand -group tsi /tb_tsi/cfg_wr_en
add wave -noupdate -expand -group tsi -radix unsigned -childformat {{/tb_tsi/data_in(65) -radix unsigned} {/tb_tsi/data_in(64) -radix unsigned} {/tb_tsi/data_in(63) -radix unsigned} {/tb_tsi/data_in(62) -radix unsigned} {/tb_tsi/data_in(61) -radix unsigned} {/tb_tsi/data_in(60) -radix unsigned} {/tb_tsi/data_in(59) -radix unsigned} {/tb_tsi/data_in(58) -radix unsigned} {/tb_tsi/data_in(57) -radix unsigned} {/tb_tsi/data_in(56) -radix unsigned} {/tb_tsi/data_in(55) -radix unsigned} {/tb_tsi/data_in(54) -radix unsigned} {/tb_tsi/data_in(53) -radix unsigned} {/tb_tsi/data_in(52) -radix unsigned} {/tb_tsi/data_in(51) -radix unsigned} {/tb_tsi/data_in(50) -radix unsigned} {/tb_tsi/data_in(49) -radix unsigned} {/tb_tsi/data_in(48) -radix unsigned} {/tb_tsi/data_in(47) -radix unsigned} {/tb_tsi/data_in(46) -radix unsigned} {/tb_tsi/data_in(45) -radix unsigned} {/tb_tsi/data_in(44) -radix unsigned} {/tb_tsi/data_in(43) -radix unsigned} {/tb_tsi/data_in(42) -radix unsigned} {/tb_tsi/data_in(41) -radix unsigned} {/tb_tsi/data_in(40) -radix unsigned} {/tb_tsi/data_in(39) -radix unsigned} {/tb_tsi/data_in(38) -radix unsigned} {/tb_tsi/data_in(37) -radix unsigned} {/tb_tsi/data_in(36) -radix unsigned} {/tb_tsi/data_in(35) -radix unsigned} {/tb_tsi/data_in(34) -radix unsigned} {/tb_tsi/data_in(33) -radix unsigned} {/tb_tsi/data_in(32) -radix unsigned} {/tb_tsi/data_in(31) -radix unsigned} {/tb_tsi/data_in(30) -radix unsigned} {/tb_tsi/data_in(29) -radix unsigned} {/tb_tsi/data_in(28) -radix unsigned} {/tb_tsi/data_in(27) -radix unsigned} {/tb_tsi/data_in(26) -radix unsigned} {/tb_tsi/data_in(25) -radix unsigned} {/tb_tsi/data_in(24) -radix unsigned} {/tb_tsi/data_in(23) -radix unsigned} {/tb_tsi/data_in(22) -radix unsigned} {/tb_tsi/data_in(21) -radix unsigned} {/tb_tsi/data_in(20) -radix unsigned} {/tb_tsi/data_in(19) -radix unsigned} {/tb_tsi/data_in(18) -radix unsigned} {/tb_tsi/data_in(17) -radix unsigned} {/tb_tsi/data_in(16) -radix unsigned} {/tb_tsi/data_in(15) -radix unsigned} {/tb_tsi/data_in(14) -radix unsigned} {/tb_tsi/data_in(13) -radix unsigned} {/tb_tsi/data_in(12) -radix unsigned} {/tb_tsi/data_in(11) -radix unsigned} {/tb_tsi/data_in(10) -radix unsigned} {/tb_tsi/data_in(9) -radix unsigned} {/tb_tsi/data_in(8) -radix unsigned} {/tb_tsi/data_in(7) -radix unsigned} {/tb_tsi/data_in(6) -radix unsigned} {/tb_tsi/data_in(5) -radix unsigned} {/tb_tsi/data_in(4) -radix unsigned} {/tb_tsi/data_in(3) -radix unsigned} {/tb_tsi/data_in(2) -radix unsigned} {/tb_tsi/data_in(1) -radix unsigned} {/tb_tsi/data_in(0) -radix unsigned}} -subitemconfig {/tb_tsi/data_in(65) {-height 16 -radix unsigned} /tb_tsi/data_in(64) {-height 16 -radix unsigned} /tb_tsi/data_in(63) {-height 16 -radix unsigned} /tb_tsi/data_in(62) {-height 16 -radix unsigned} /tb_tsi/data_in(61) {-height 16 -radix unsigned} /tb_tsi/data_in(60) {-height 16 -radix unsigned} /tb_tsi/data_in(59) {-height 16 -radix unsigned} /tb_tsi/data_in(58) {-height 16 -radix unsigned} /tb_tsi/data_in(57) {-height 16 -radix unsigned} /tb_tsi/data_in(56) {-height 16 -radix unsigned} /tb_tsi/data_in(55) {-height 16 -radix unsigned} /tb_tsi/data_in(54) {-height 16 -radix unsigned} /tb_tsi/data_in(53) {-height 16 -radix unsigned} /tb_tsi/data_in(52) {-height 16 -radix unsigned} /tb_tsi/data_in(51) {-height 16 -radix unsigned} /tb_tsi/data_in(50) {-height 16 -radix unsigned} /tb_tsi/data_in(49) {-height 16 -radix unsigned} /tb_tsi/data_in(48) {-height 16 -radix unsigned} /tb_tsi/data_in(47) {-height 16 -radix unsigned} /tb_tsi/data_in(46) {-height 16 -radix unsigned} /tb_tsi/data_in(45) {-height 16 -radix unsigned} /tb_tsi/data_in(44) {-height 16 -radix unsigned} /tb_tsi/data_in(43) {-height 16 -radix unsigned} /tb_tsi/data_in(42) {-height 16 -radix unsigned} /tb_tsi/data_in(41) {-height 16 -radix unsigned} /tb_tsi/data_in(40) {-height 16 -radix unsigned} /tb_tsi/data_in(39) {-height 16 -radix unsigned} /tb_tsi/data_in(38) {-height 16 -radix unsigned} /tb_tsi/data_in(37) {-height 16 -radix unsigned} /tb_tsi/data_in(36) {-height 16 -radix unsigned} /tb_tsi/data_in(35) {-height 16 -radix unsigned} /tb_tsi/data_in(34) {-height 16 -radix unsigned} /tb_tsi/data_in(33) {-height 16 -radix unsigned} /tb_tsi/data_in(32) {-height 16 -radix unsigned} /tb_tsi/data_in(31) {-height 16 -radix unsigned} /tb_tsi/data_in(30) {-height 16 -radix unsigned} /tb_tsi/data_in(29) {-height 16 -radix unsigned} /tb_tsi/data_in(28) {-height 16 -radix unsigned} /tb_tsi/data_in(27) {-height 16 -radix unsigned} /tb_tsi/data_in(26) {-height 16 -radix unsigned} /tb_tsi/data_in(25) {-height 16 -radix unsigned} /tb_tsi/data_in(24) {-height 16 -radix unsigned} /tb_tsi/data_in(23) {-height 16 -radix unsigned} /tb_tsi/data_in(22) {-height 16 -radix unsigned} /tb_tsi/data_in(21) {-height 16 -radix unsigned} /tb_tsi/data_in(20) {-height 16 -radix unsigned} /tb_tsi/data_in(19) {-height 16 -radix unsigned} /tb_tsi/data_in(18) {-height 16 -radix unsigned} /tb_tsi/data_in(17) {-height 16 -radix unsigned} /tb_tsi/data_in(16) {-height 16 -radix unsigned} /tb_tsi/data_in(15) {-height 16 -radix unsigned} /tb_tsi/data_in(14) {-height 16 -radix unsigned} /tb_tsi/data_in(13) {-height 16 -radix unsigned} /tb_tsi/data_in(12) {-height 16 -radix unsigned} /tb_tsi/data_in(11) {-height 16 -radix unsigned} /tb_tsi/data_in(10) {-height 16 -radix unsigned} /tb_tsi/data_in(9) {-height 16 -radix unsigned} /tb_tsi/data_in(8) {-height 16 -radix unsigned} /tb_tsi/data_in(7) {-height 16 -radix unsigned} /tb_tsi/data_in(6) {-height 16 -radix unsigned} /tb_tsi/data_in(5) {-height 16 -radix unsigned} /tb_tsi/data_in(4) {-height 16 -radix unsigned} /tb_tsi/data_in(3) {-height 16 -radix unsigned} /tb_tsi/data_in(2) {-height 16 -radix unsigned} /tb_tsi/data_in(1) {-height 16 -radix unsigned} /tb_tsi/data_in(0) {-height 16 -radix unsigned}} /tb_tsi/data_in
add wave -noupdate -expand -group tsi -radix unsigned /tb_tsi/data_out
add wave -noupdate -group control /tb_tsi/uut/control_unit/clk
add wave -noupdate -group control /tb_tsi/uut/control_unit/reset
add wave -noupdate -group control /tb_tsi/uut/control_unit/data_valid
add wave -noupdate -group control /tb_tsi/uut/control_unit/cfg_data
add wave -noupdate -group control /tb_tsi/uut/control_unit/cfg
add wave -noupdate -group control -radix unsigned /tb_tsi/uut/control_unit/cfg_raddr
add wave -noupdate -group control /tb_tsi/uut/control_unit/cfg_port_en
add wave -noupdate -group control /tb_tsi/uut/control_unit/data_waddr
add wave -noupdate -group control -radix unsigned /tb_tsi/uut/control_unit/data_raddr
add wave -noupdate -group control /tb_tsi/uut/control_unit/data_write_port_en
add wave -noupdate -group control /tb_tsi/uut/control_unit/data_read_port_en
add wave -noupdate -group control /tb_tsi/uut/control_unit/data_wr_en
add wave -noupdate -group control -radix unsigned /tb_tsi/uut/control_unit/waddr_count
add wave -noupdate -group control -radix unsigned /tb_tsi/uut/control_unit/raddr_count
add wave -noupdate -group control /tb_tsi/uut/control_unit/begin_read
add wave -noupdate -group control /tb_tsi/uut/control_unit/ram_block_part
add wave -noupdate /tb_tsi/uut/rdy
add wave -noupdate /tb_tsi/uut/control_unit/clk
add wave -noupdate /tb_tsi/uut/control_unit/reset
add wave -noupdate /tb_tsi/uut/control_unit/data_valid
add wave -noupdate /tb_tsi/uut/control_unit/cfg_data
add wave -noupdate /tb_tsi/uut/control_unit/cfg
add wave -noupdate /tb_tsi/uut/control_unit/cfg_raddr
add wave -noupdate /tb_tsi/uut/control_unit/cfg_port_en
add wave -noupdate -radix decimal /tb_tsi/uut/control_unit/data_waddr
add wave -noupdate -radix decimal /tb_tsi/uut/control_unit/data_raddr
add wave -noupdate /tb_tsi/uut/control_unit/data_write_port_en
add wave -noupdate /tb_tsi/uut/control_unit/data_read_port_en
add wave -noupdate /tb_tsi/uut/control_unit/rdy
add wave -noupdate /tb_tsi/uut/control_unit/new_cfg
add wave -noupdate /tb_tsi/uut/control_unit/data_wr_en
add wave -noupdate -radix decimal /tb_tsi/uut/control_unit/waddr_count
add wave -noupdate -radix decimal /tb_tsi/uut/control_unit/raddr_count
add wave -noupdate /tb_tsi/uut/control_unit/int_raddr
add wave -noupdate /tb_tsi/uut/control_unit/ram_block_part
add wave -noupdate /tb_tsi/uut/control_unit/begin_read
add wave -noupdate /tb_tsi/uut/control_unit/int_rdy
add wave -noupdate /tb_tsi/uut/control_unit/int_new_cfg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {762454 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 176
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {656165 ps} {918665 ps}
