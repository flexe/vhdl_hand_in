echo Compile vhd files
vcom src/data_gen.vhd src/tb_flexe_top.vhd src/flexe_top.vhd src/tsi/* src/crossbar/*


echo starting tb simulation
vsim work.tb_flexe_top


echo loading preconfigured memory
#do load_mem_linux.do
#do load_mem_windows.do
#do load_mem_windows_final.do
do load_mem_linux_final.do





echo adding wave signals
#do tb_tx_top_wave.do
do tb_flexe_top_wave.do


echo running for 6000 ns
run 4000 ns
wave zoom full
#wave zoom in 2.5
