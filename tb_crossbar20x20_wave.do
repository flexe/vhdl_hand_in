onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /tb_crossbar20x20/i0
add wave -noupdate -radix unsigned /tb_crossbar20x20/i1
add wave -noupdate -radix unsigned /tb_crossbar20x20/i2
add wave -noupdate -radix unsigned /tb_crossbar20x20/i3
add wave -noupdate -radix unsigned /tb_crossbar20x20/i4
add wave -noupdate -radix unsigned /tb_crossbar20x20/i5
add wave -noupdate -radix unsigned /tb_crossbar20x20/i6
add wave -noupdate -radix unsigned /tb_crossbar20x20/i7
add wave -noupdate -radix unsigned /tb_crossbar20x20/i8
add wave -noupdate -radix unsigned /tb_crossbar20x20/i9
add wave -noupdate -radix unsigned /tb_crossbar20x20/i10
add wave -noupdate -radix unsigned /tb_crossbar20x20/i11
add wave -noupdate -radix unsigned /tb_crossbar20x20/i12
add wave -noupdate -radix unsigned /tb_crossbar20x20/i13
add wave -noupdate -radix unsigned /tb_crossbar20x20/i14
add wave -noupdate -radix unsigned /tb_crossbar20x20/i15
add wave -noupdate -radix unsigned /tb_crossbar20x20/i16
add wave -noupdate -radix unsigned /tb_crossbar20x20/i17
add wave -noupdate -radix unsigned /tb_crossbar20x20/i18
add wave -noupdate -radix unsigned /tb_crossbar20x20/i19
add wave -noupdate -radix unsigned /tb_crossbar20x20/o0
add wave -noupdate -radix unsigned /tb_crossbar20x20/o1
add wave -noupdate -radix unsigned /tb_crossbar20x20/o2
add wave -noupdate -radix unsigned /tb_crossbar20x20/o3
add wave -noupdate -radix unsigned /tb_crossbar20x20/o4
add wave -noupdate -radix unsigned /tb_crossbar20x20/o5
add wave -noupdate -radix unsigned /tb_crossbar20x20/o6
add wave -noupdate -radix unsigned /tb_crossbar20x20/o7
add wave -noupdate -radix unsigned /tb_crossbar20x20/o8
add wave -noupdate -radix unsigned /tb_crossbar20x20/o9
add wave -noupdate -radix unsigned /tb_crossbar20x20/o10
add wave -noupdate -radix unsigned /tb_crossbar20x20/o11
add wave -noupdate -radix unsigned /tb_crossbar20x20/o12
add wave -noupdate -radix unsigned /tb_crossbar20x20/o13
add wave -noupdate -radix unsigned /tb_crossbar20x20/o14
add wave -noupdate -radix unsigned /tb_crossbar20x20/o15
add wave -noupdate -radix unsigned /tb_crossbar20x20/o16
add wave -noupdate -radix unsigned /tb_crossbar20x20/o17
add wave -noupdate -radix unsigned /tb_crossbar20x20/o18
add wave -noupdate -radix unsigned /tb_crossbar20x20/o19
add wave -noupdate /tb_crossbar20x20/sel
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {99240 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {99050 ps} {100050 ps}
