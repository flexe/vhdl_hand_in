-------------------------------------------------------------------------
-- Test bench for flexe top file
-- Two calendar components
-- TX --> RX
-- Anders Jørgensen
-------------------------------------------------------------------------


library ieee;
library modelsim_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.math_real.all;
use modelsim_lib.util.all;



entity tb_flexe_top is
end tb_flexe_top;

architecture behavior of tb_flexe_top is

component flexe_top is
    port ( clk : in std_logic;
           reset : in std_logic;
           --data_valid : in std_logic;
           cfg_stage1 : in std_logic;
           cfg_stage2 : in std_logic;
           cfg_stage3 : in std_logic;
           data_in : in std_logic_vector(1319 downto 0);
           data_out : out std_logic_vector(1319 downto 0);
           new_cfg : out std_logic;
           data_valid : in std_logic
           );
  end component;


   component data_gen is
     port ( clk : in std_logic;
            reset : in std_logic;
            start : in std_logic;
            data_out : out std_logic_vector
            );
   end component;



signal clk : std_logic;
signal reset : std_logic;
signal data_valid : std_logic;
signal flex_valid : std_logic;
signal data_in : std_logic_vector(1319 downto 0);
signal data_out : std_logic_vector(1319 downto 0);
signal data_flex_e : std_logic_vector(1319 downto 0);

signal int_data_gen : std_logic_vector(1319 downto 0);
signal data_2 : std_logic_vector(1319 downto 0);
signal start_2 : std_logic;

signal int_cfg_stage1 : std_logic;
signal int_cfg_stage2 : std_logic;
signal int_cfg_stage3 : std_logic;

signal int_cfg_stage1_rx : std_logic;
signal int_cfg_stage2_rx : std_logic;
signal int_cfg_stage3_rx : std_logic;

signal check : std_logic;



  constant CLK_PERIOD : time := 10 ns;



begin



  tx : flexe_top PORT MAP (
          clk => clk,
          reset => reset,
          data_valid => data_valid,
          data_in => int_data_gen,
          cfg_stage1 => int_cfg_stage1,
          cfg_stage2 => int_cfg_stage2,
          cfg_stage3 => int_cfg_stage3,
          data_out => data_flex_e
      );


  rx : flexe_top PORT MAP (
        clk => clk,
        reset => reset,
        data_in => data_flex_e,
        data_valid => flex_valid,
        cfg_stage1 => int_cfg_stage1_rx,
        cfg_stage2 => int_cfg_stage2_rx,
        cfg_stage3 => int_cfg_stage3_rx,
        data_out => data_out
  );

     data_gen_0 : data_gen PORT MAP(
            clk => clk,
            reset => reset,
            start => data_valid,
            data_out => int_data_gen
       );


    data_gen_1 : data_gen PORT MAP(
            clk => clk,
            reset => reset,
            start => start_2,
            data_out => data_2
    );


clk_process :process
  begin
    clk <= '0';
      wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
      clk <= '1';
      wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
end process;


reset_process : process
begin
  --wait for 10 ns;
  reset <= '1';
  wait for 10 ns;
  reset <= '0';
  wait;
end process;





-- data gen
data_gen_process : process

  begin
    data_valid <= '0';
    wait for 10 ns;
    data_valid <= '1';
    wait;



  end process;


  cfg_process : process

  begin

    int_cfg_stage1 <= '0';
    int_cfg_stage2 <= '0';
    int_cfg_stage3 <= '0';

    int_cfg_stage1_rx <= '0';
    int_cfg_stage2_rx <= '0';
    int_cfg_stage3_rx <= '0';


    start_2 <= '0';
    flex_valid <= '0';
    wait for 1645 ns;
    flex_valid <= '1';
    wait for 1610 ns;
    start_2 <= '1';
    --int_cfg_stage1 <= '1';
    --int_cfg_stage2 <= '1';
    wait for 800 ns;
    --int_cfg_stage3 <= '1';
    wait for 800 ns;
    ---int_cfg_stage1_rx <= '1';
    --int_cfg_stage2_rx <= '1';
    wait for 800 ns;
    --int_cfg_stage3_rx <= '1';
    --wait for
    wait;

  end process;


check <= '1' when (data_out = data_2) else '0';



end;
