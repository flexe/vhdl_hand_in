-------------------------------------------------------------------------
-- Topfile for flexE calendar
-- TST swtich architecture
-- 20xTSI --> 20X20Crossbar --> 20xTSI + 1 crossbar control
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;



entity flexe_top is
  port ( clk : in std_logic;
         reset : in std_logic;
         cfg_stage1 : in std_logic;
         cfg_stage2 : in std_logic;
         cfg_stage3 : in std_logic;
         --data_valid : in std_logic;
         data_in : in std_logic_vector(1319 downto 0);
         data_out : out std_logic_vector(1319 downto 0);
         new_cfg : out std_logic;
         data_valid : in std_logic
         );
end flexe_top;


architecture behavioral of flexe_top is

-------------------------------------------------------------------------
--                       Components
-------------------------------------------------------------------------

component tsi is
port (clk : in std_logic;
      reset : in std_logic;
      data_valid : in std_logic;
      cfg_data : in std_logic_vector(7 downto 0);
      cfg_wr_en : in std_logic;
      cfg : in std_logic;
      cfg_waddr : in std_logic_vector(7 downto 0);
      data_in : in std_logic_vector(65 downto 0);
      rdy : out std_logic;
      new_cfg : out std_logic;
      data_out : out std_logic_vector(65 downto 0)
      );
end component;


component crossbar20x20
port ( i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : in std_logic_vector(65 downto 0);
       sel : in std_logic_vector(99 downto 0);
       o0,o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19   : out std_logic_vector(65 downto 0)
       );
end component;


component ctl_crossbar is
  port ( clk : in std_logic;
         reset : in std_logic;
         data_valid : in std_logic;
         cfg : in std_logic;
         sel : out std_logic_vector(99 downto 0)
         );
end component;




-------------------------------------------------------------------------
--                    Internal signals
-------------------------------------------------------------------------

 signal int_tsi_cb : std_logic_vector(1319 downto 0); -- data from tsi(stage1) to crossbar(stage2)
 signal int_cb_tsi : std_logic_vector(1319 downto 0); -- data from crossbar(stage2) to tsi(stage3)

 signal int_sel : std_logic_vector(99 downto 0); --Crossbar selctor signal from ctl_crossbar to crossbar

 signal int_cfg_stage1 : std_logic; -- configuration determine for stage1
 signal int_cfg_stage2 : std_logic; -- configuration determine for stage2
 signal int_cfg_stage3 : std_logic; -- configuration determine for stage3

 signal int_data_valid_stage1 : std_logic; -- data_valid from stage1, signal to start crossbar
 signal int_data_valid_stage2 : std_logic; -- data_valid from stage2, signal to start tsi at third stage
 signal int_data_valid_stage3 : std_logic; -- data_valid from stage3, signal to known where the the output FlexE signal is valid

 signal int_rdy_stage1 : std_logic; -- stage1 is starting to read from data memory, start next stage (2+3)

 signal int_new_cfg : std_logic; -- pulse to indicate, when a config can be changed.



begin

-- --
-- s3_valid : process (clk, reset, int_rdy_stage1)
-- begin
--
--   if(reset='1') then
--     int_data_valid_stage3 <= '0';
--     int_data_valid_stage2 <= '0';
--     --int_data_valid_stage3 <= '0';
--   elsif(rising_edge(clk)) then
--     if (int_rdy_stage1='1') then
--       int_data_valid_stage3 <= '1';
--       int_data_valid_stage2 <= '1';
--       --int_data_valid_stage3 <= '1';
--     end if;
--   end if;
-- end process;

-------------------------------------------------------------------------
--                Internal signal assignment etc.
------------------------------------------------------------------------

int_data_valid_stage1 <= '1' when data_valid = '1' else '0';
int_data_valid_stage2 <= int_rdy_stage1;
int_data_valid_stage3 <= int_rdy_stage1;

int_cfg_stage1 <= cfg_stage1;
int_cfg_stage2 <= cfg_stage2;
int_cfg_stage3 <= cfg_stage3;

new_cfg <= int_new_cfg;




-------------------------------------------------------------------------
--                      Control Unit for crossbar
-------------------------------------------------------------------------

  ctl_crossbar0 : ctl_crossbar PORT MAP(
      clk => clk,
      reset => reset,
      data_valid => int_data_valid_stage2,
      cfg => int_cfg_stage2,
      sel => int_sel
  );


-------------------------------------------------------------------------
--                     TSI's stage 1
-------------------------------------------------------------------------

  tsi0 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(1319 downto 1254),
           rdy => int_rdy_stage1,
           new_cfg => int_new_cfg,
           data_out => int_tsi_cb(1319 downto 1254)
     );

  tsi1 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(1253 downto 1188),
           data_out => int_tsi_cb(1253 downto 1188)
     );

  tsi2 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(1187 downto 1122),
           data_out => int_tsi_cb(1187 downto 1122)
     );

  tsi3 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(1121 downto 1056),
           data_out => int_tsi_cb(1121 downto 1056)
     );

  tsi4 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(1055 downto 990),
           data_out => int_tsi_cb(1055 downto 990)
     );

  tsi5 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(989 downto 924),
           data_out => int_tsi_cb(989 downto 924)
     );

  tsi6 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(923 downto 858),
           data_out => int_tsi_cb(923 downto 858)
     );

  tsi7 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(857 downto 792),
           data_out => int_tsi_cb(857 downto 792)
     );

  tsi8 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(791 downto 726),
           data_out => int_tsi_cb(791 downto 726)
     );

  tsi9 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(725 downto 660),
           data_out => int_tsi_cb(725 downto 660)
     );

  tsi10 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(659 downto 594),
           data_out => int_tsi_cb(659 downto 594)
     );

  tsi11 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(593 downto 528),
           data_out => int_tsi_cb(593 downto 528)
     );

  tsi12 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(527 downto 462),
           data_out => int_tsi_cb(527 downto 462)
     );

  tsi13 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(461 downto 396),
           data_out => int_tsi_cb(461 downto 396)
     );

  tsi14 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(395 downto 330),
           data_out => int_tsi_cb(395 downto 330)
     );

  tsi15 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(329 downto 264),
           data_out => int_tsi_cb(329 downto 264)
     );

  tsi16 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(263 downto 198),
           data_out => int_tsi_cb(263 downto 198)
     );

  tsi17 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(197 downto 132),
           data_out => int_tsi_cb(197 downto 132)
     );

  tsi18 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(131 downto 66),
           data_out => int_tsi_cb(131 downto 66)
     );

  tsi19 : tsi PORT MAP (
           clk => clk,
           reset => reset,
           data_valid => int_data_valid_stage1,
           cfg => int_cfg_stage1,
           cfg_wr_en => '0',
           cfg_data => (others => '0'),
           cfg_waddr => (others => '0'),
           data_in => data_in(65 downto 0),
           data_out => int_tsi_cb(65 downto 0)
     );



-------------------------------------------------------------------------
--                    20x20 Crossbar stage 2
-------------------------------------------------------------------------

  crossbar : crossbar20x20 PORT MAP(
    i0 => int_tsi_cb(1319 downto 1254),
    i1 => int_tsi_cb(1253 downto 1188),
    i2 => int_tsi_cb(1187 downto 1122),
    i3 => int_tsi_cb(1121 downto 1056),
    i4 => int_tsi_cb(1055 downto 990),
    i5 => int_tsi_cb(989 downto 924),
    i6 => int_tsi_cb(923 downto 858),
    i7 => int_tsi_cb(857 downto 792),
    i8 => int_tsi_cb(791 downto 726),
    i9 => int_tsi_cb(725 downto 660),
    i10 => int_tsi_cb(659 downto 594),
    i11 => int_tsi_cb(593 downto 528),
    i12 => int_tsi_cb(527 downto 462),
    i13 => int_tsi_cb(461 downto 396),
    i14 => int_tsi_cb(395 downto 330),
    i15 => int_tsi_cb(329 downto 264),
    i16 => int_tsi_cb(263 downto 198),
    i17 => int_tsi_cb(197 downto 132),
    i18 => int_tsi_cb(131 downto 66),
    i19 => int_tsi_cb(65 downto 0),
    o0 => int_cb_tsi(1319 downto 1254),
    o1 => int_cb_tsi(1253 downto 1188),
    o2 => int_cb_tsi(1187 downto 1122),
    o3 => int_cb_tsi(1121 downto 1056),
    o4 => int_cb_tsi(1055 downto 990),
    o5 => int_cb_tsi(989 downto 924),
    o6 => int_cb_tsi(923 downto 858),
    o7 => int_cb_tsi(857 downto 792),
    o8 => int_cb_tsi(791 downto 726),
    o9 => int_cb_tsi(725 downto 660),
    o10 => int_cb_tsi(659 downto 594),
    o11 => int_cb_tsi(593 downto 528),
    o12 => int_cb_tsi(527 downto 462),
    o13 => int_cb_tsi(461 downto 396),
    o14 => int_cb_tsi(395 downto 330),
    o15 => int_cb_tsi(329 downto 264),
    o16 => int_cb_tsi(263 downto 198),
    o17 => int_cb_tsi(197 downto 132),
    o18 => int_cb_tsi(131 downto 66),
    o19 => int_cb_tsi(65 downto 0),
    sel => int_sel
  );


-------------------------------------------------------------------------
--                       TSI's stage 3
-------------------------------------------------------------------------

tsi20 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(1319 downto 1254),
         data_out => data_out(1319 downto 1254)
   );

tsi21 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(1253 downto 1188),
         data_out => data_out(1253 downto 1188)
   );

tsi22 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(1187 downto 1122),
         data_out => data_out(1187 downto 1122)
   );

tsi23 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(1121 downto 1056),
         data_out => data_out(1121 downto 1056)
   );

tsi24 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(1055 downto 990),
         data_out => data_out(1055 downto 990)
   );

tsi25 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(989 downto 924),
         data_out => data_out(989 downto 924)
   );

tsi26 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(923 downto 858),
         data_out => data_out(923 downto 858)
   );

tsi27 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(857 downto 792),
         data_out => data_out(857 downto 792)
   );

tsi28 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(791 downto 726),
         data_out => data_out(791 downto 726)
   );

tsi29 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(725 downto 660),
         data_out => data_out(725 downto 660)
   );

tsi30 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(659 downto 594),
         data_out => data_out(659 downto 594)
   );

tsi31 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(593 downto 528),
         data_out => data_out(593 downto 528)
   );

tsi32 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(527 downto 462),
         data_out => data_out(527 downto 462)
   );

tsi33 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(461 downto 396),
         data_out => data_out(461 downto 396)
   );

tsi34 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(395 downto 330),
         data_out => data_out(395 downto 330)
   );

tsi35 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(329 downto 264),
         data_out => data_out(329 downto 264)
   );

tsi36 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(263 downto 198),
         data_out => data_out(263 downto 198)
   );

tsi37 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(197 downto 132),
         data_out => data_out(197 downto 132)
   );

tsi38 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(131 downto 66),
         data_out => data_out(131 downto 66)
   );

tsi39 : tsi PORT MAP (
         clk => clk,
         reset => reset,
         data_valid => int_data_valid_stage3,
         cfg => int_cfg_stage3,
         cfg_wr_en => '0',
         cfg_data => (others => '0'),
         cfg_waddr => (others => '0'),
         data_in => int_cb_tsi(65 downto 0),
         data_out => data_out(65 downto 0)
   );






end;
