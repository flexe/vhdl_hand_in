-------------------------------------------------------------------------
-- Simple data generation - counter 0 to 79
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity data_gen is
 port( clk : in std_logic;
       reset : in std_logic;
       start : in std_logic;
       data_out : out std_logic_vector(1319 downto 0)
       );
end entity;

architecture behavioral of data_gen is


signal counter : std_logic_vector(65 downto 0) := (others => '0');



begin



  cnt : process(clk,reset)
  begin
    if(reset='1') then
      counter <= (others => '0');
    elsif(rising_edge(clk)) then
      if (start = '1') then
        if (counter = "000000000000000000000000000000000000000000000000000000000001001111") then
          counter <= (others => '0');
        else
          counter <= counter + 1;
        end if;
      end if;
    end if;

  end process;


--data_out(1319 downto 66) <= (others => '0');
--data_out(65 downto 0) <= counter;

  data_out(1319 downto 1254) <= counter;
  data_out(1253 downto 1188) <= counter;
  data_out(1187 downto 1122) <= counter;
  data_out(1121 downto 1056) <= counter;
  data_out(1055 downto 990) <= counter;
  data_out(989 downto 924) <= counter;
  data_out(923 downto 858) <= counter;
  data_out(857 downto 792) <= counter;
  data_out(791 downto 726) <= counter;
  data_out(725 downto 660) <= counter;
  data_out(659 downto 594) <= counter;
  data_out(593 downto 528) <= counter;
  data_out(527 downto 462) <= counter;
  data_out(461 downto 396) <= counter;
  data_out(395 downto 330) <= counter;
  data_out(329 downto 264) <= counter;
  data_out(263 downto 198) <= counter;
  data_out(197 downto 132) <= counter;
  data_out(131 downto 66) <= counter;
  data_out(65 downto 0) <= counter;





end;
