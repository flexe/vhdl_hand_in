library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- entity declaration for your testbench.
--Notice that the entity port list is empty here.
entity tb_control is
end tb_control;

architecture behavior of tb_control is

-- component declaration for the unit under test (uut)
component control is
port (clk : in std_logic;
      reset : in std_logic;
      data_valid : in std_logic;
      waddr : out std_logic_vector(7 downto 0);
      raddr : out std_logic_vector(7 downto 0);
      port_en_0 : out std_logic;
      port_en_1 : out std_logic;
      wr_en : out std_logic);
end component;

--declare inputs and initialize them to zero.
signal clk : std_logic := '0';
signal reset : std_logic := '0';
signal data_valid : std_logic := '0';


--declare outputs
signal port_en_0 : std_logic;
signal port_en_1 : std_logic;
signal wr_en : std_logic;
signal waddr : std_logic_vector(7 downto 0);
signal raddr : std_logic_vector(7 downto 0);

-- define the period of clock here.
-- It's recommended to use CAPITAL letters to define constants.
constant CLK_PERIOD : time := 10 ns;

begin



    -- instantiate the unit under test (uut)
    uut : control PORT MAP (
            clk => clk,
            reset => reset,
            data_valid => data_valid,
            waddr => waddr,
            raddr => raddr,
            port_en_0 => port_en_0,
            port_en_1 => port_en_1,
            wr_en => wr_en
        );






   -- Clock process definitions( clock with 50% duty cycle is generated here.
   clk_process :process
   begin
        clk <= '0';
        wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
        clk <= '1';
        wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
   end process;

   -- Stimulus process, Apply inputs here.
  stim_proc: process
   begin
        --wait for CLK_PERIOD*10; --wait for 10 clock cycles.
        --reset <='1';                    --then apply reset for 2 clock cycles.
        --wait for CLK_PERIOD*2;
        reset <='0';                    --then pull down reset for 20 clock cycles.
        wait for CLK_PERIOD;
        reset <= '1';               --then apply reset for one clock cycle.
        wait for CLK_PERIOD;
        reset <= '0';               --pull down reset and let the counter run.
        wait for CLK_PERIOD;
        data_valid <= '1';
        --wait for CLK_PERIOD*20;
        --cnt_en <= '0';
        --wait for CLK_PERIOD*5;
        --cnt_en <= '1';
        wait;
  end process;

end;
