library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;


entity tb_crossbar20x20 is
end tb_crossbar20x20;



architecture behavioral of tb_crossbar20x20 is


  component crossbar20x20
  port ( i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : in std_logic_vector(65 downto 0);
         sel : in std_logic_vector(99 downto 0);
         o0,o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19   : out std_logic_vector(65 downto 0)
         );
  end component;

  signal i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : std_logic_vector(65 downto 0);
  signal o0,o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19 : std_logic_vector(65 downto 0);
  signal sel : std_logic_vector(99 downto 0);



  begin

    crossbar20x20_1 : crossbar20x20 PORT MAP(
    i0 => i0,
i1 => i1,
i2 => i2,
i3 => i3,
i4 => i4,
i5 => i5,
i6 => i6,
i7 => i7,
i8 => i8,
i9 => i9,
i10 => i10,
i11 => i11,
i12 => i12,
i13 => i13,
i14 => i14,
i15 => i15,
i16 => i16,
i17 => i17,
i18 => i18,
i19 => i19,
o0 => o0,
o1 => o1,
o2 => o2,
o3 => o3,
o4 => o4,
o5 => o5,
o6 => o6,
o7 => o7,
o8 => o8,
o9 => o9,
o10 => o10,
o11 => o11,
o12 => o12,
o13 => o13,
o14 => o14,
o15 => o15,
o16 => o16,
o17 => o17,
o18 => o18,
o19 => o19,
sel => sel
    );

    data_gen : process

    begin

      i0 <= conv_std_logic_vector(0,66);
      i1 <= conv_std_logic_vector(1,66);
      i2 <= conv_std_logic_vector(2,66);
      i3 <= conv_std_logic_vector(3,66);
      i4 <= conv_std_logic_vector(4,66);
      i5 <= conv_std_logic_vector(5,66);
      i6 <= conv_std_logic_vector(6,66);
      i7 <= conv_std_logic_vector(7,66);
      i8 <= conv_std_logic_vector(8,66);
      i9 <= conv_std_logic_vector(9,66);
      i10 <= conv_std_logic_vector(10,66);
      i11 <= conv_std_logic_vector(11,66);
      i12 <= conv_std_logic_vector(12,66);
      i13 <= conv_std_logic_vector(13,66);
      i14 <= conv_std_logic_vector(14,66);
      i15 <= conv_std_logic_vector(15,66);
      i16 <= conv_std_logic_vector(16,66);
      i17 <= conv_std_logic_vector(17,66);
      i18 <= conv_std_logic_vector(18,66);
      i19 <= conv_std_logic_vector(19,66);
      wait for 10 ns;

      sel <= "1001110010100011000001111011100110101100010110101001001010000011100110001010010000011000100000100000";
      wait for 50 ns;
      sel <= "0000000001000100001100100001010011000111010000100101010010110110001101011100111110000100011001010011";

      wait;



    end process;





end;
