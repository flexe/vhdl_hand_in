library ieee;
library modelsim_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.math_real.all;
use modelsim_lib.util.all;


entity tb_ctl_crossbar is
end tb_ctl_crossbar;


architecture behavior of tb_ctl_crossbar is


  component ctl_crossbar is
    port ( clk : in std_logic;
           reset : in std_logic;
           data_valid : in std_logic;
           cfg : in std_logic;
           sel : out std_logic_vector(99 downto 0)
           );
  end component;


  signal clk : std_logic := '0';
  signal reset : std_logic := '0';
  signal data_valid : std_logic := '0';
  signal cfg : std_logic := '0';
  signal sel : std_logic_vector(99 downto 0);


  constant CLK_PERIOD : time := 10 ns;



  begin

      -- instantiate the unit under test (uut)
      uut : ctl_crossbar PORT MAP (
              clk => clk,
              reset => reset,
              data_valid => data_valid,
              cfg => cfg,
              sel => sel
          );


          clk_process :process
            begin
              clk <= '0';
                wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
                clk <= '1';
                wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
          end process;


        reset_process : process
          begin
            wait for 10 ns;
            reset <= '1';
            wait for 10 ns;
            reset <= '0';
            wait;
          end process;

          data_valid_process : process
            begin
             wait for 20 ns;
             data_valid <= '1';
             wait;
           end process;

end;
