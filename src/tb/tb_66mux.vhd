library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;


entity tb_mux66_20to1 is
end tb_mux66_20to1;



architecture behavioral of tb_mux66_20to1 is

component mux_66b_20to1
port ( sel : in std_logic_vector(4 downto 0);
       i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : in std_logic_vector(65 downto 0);
       o   : out std_logic_vector(65 downto 0)
       );
end component;


  signal i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : std_logic_vector(65 downto 0);
  signal sel : std_logic_vector(4 downto 0);


  signal o : std_logic_vector(65 downto 0);

begin


  uut : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel,
    o => o
  );


  stim_proc: process

  begin

    --set inputs:


    i0 <= conv_std_logic_vector(0,66);
    i1 <= conv_std_logic_vector(1,66);
    i2 <= conv_std_logic_vector(2,66);
    i3 <= conv_std_logic_vector(3,66);
    i4 <= conv_std_logic_vector(4,66);
    i5 <= conv_std_logic_vector(5,66);
    i6 <= conv_std_logic_vector(6,66);
    i7 <= conv_std_logic_vector(7,66);
    i8 <= conv_std_logic_vector(8,66);
    i9 <= conv_std_logic_vector(9,66);
    i10 <= conv_std_logic_vector(10,66);
    i11 <= conv_std_logic_vector(11,66);
    i12 <= conv_std_logic_vector(12,66);
    i13 <= conv_std_logic_vector(13,66);
    i14 <= conv_std_logic_vector(14,66);
    i15 <= conv_std_logic_vector(15,66);
    i16 <= conv_std_logic_vector(16,66);
    i17 <= conv_std_logic_vector(17,66);
    i18 <= conv_std_logic_vector(18,66);
    i19 <= conv_std_logic_vector(19,66);

    wait for 10 ns;
    sel <= conv_std_logic_vector(0,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(1,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(2,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(3,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(4,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(5,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(6,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(7,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(8,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(9,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(10,5);
    wait for 10 ns;
    sel <= conv_std_logic_vector(11,5);
    wait;

  end process;



-- := conv_std_logic_vector(3);



end architecture;
