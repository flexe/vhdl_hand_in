library ieee;
library modelsim_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.math_real.all;
use modelsim_lib.util.all;



entity tb_tsi is
end tb_tsi;

architecture behavior of tb_tsi is

-- component declaration for the unit under test (uut)
component tsi is
port (clk : in std_logic;
      reset : in std_logic;
      data_valid : in std_logic;
      cfg_data : in std_logic_vector(7 downto 0);
      cfg_wr_en : in std_logic;
      cfg : in std_logic;
      cfg_waddr : in std_logic_vector(7 downto 0);
      data_in : in std_logic_vector(65 downto 0);
      rdy : out std_logic;
      data_out : out std_logic_vector(65 downto 0)
      );
end component;


signal clk : std_logic := '0';
signal reset : std_logic := '0';
signal data_valid : std_logic := '0';
signal cfg : std_logic := '0';
signal cfg_wr_en : std_logic := '0';
signal cfg_waddr : std_logic_vector(7 downto 0);
signal cfg_data : std_logic_vector(7 downto 0);
signal data_in : std_logic_vector(65 downto 0);
signal data_out : std_logic_vector(65 downto 0);


-- data gen
signal rand_num : integer := 0;

--signal int_waddr : std_logic_vector(7 downto 0);
--signal int_raddr : std_logic_vector(7 downto 0);
--signal int_test : std_logic;
--signal int_data_in : std_logic_vector(65 downto 0);



constant CLK_PERIOD : time := 10 ns;




begin

    -- instantiate the unit under test (uut)
    uut : tsi PORT MAP (
            clk => clk,
            reset => reset,
            data_valid => data_valid,
            cfg => cfg,
            cfg_wr_en => cfg_wr_en,
            cfg_data => cfg_data,
            cfg_waddr => cfg_waddr,
            data_in => data_in,
            data_out => data_out
        );


  clk_process :process
    begin
      clk <= '0';
        wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
        clk <= '1';
        wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
  end process;


reset_process : process
  begin
    wait for 10 ns;
    reset <= '1';
    wait for 10 ns;
    reset <= '0';
    wait;
  end process;


-- data gen
data_gen_process : process

  variable seed1, seed2: positive;
  variable rand: real;
  variable range_of_rand : real := 1000.0;

  begin

    wait for 20 ns;
    data_valid <= '1';

    for i in 0 to 79 loop
    data_in <= conv_std_logic_vector(i,66);

    wait for 10 ns;
    end loop;

    for i in 0 to 79 loop
    data_in <= conv_std_logic_vector(i,66);

    wait for 10 ns;
    end loop;

    wait;
  end process;


end;
