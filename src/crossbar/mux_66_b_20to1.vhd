-------------------------------------------------------------------------
-- Single 66bit - 20to1 multiplexer
-- sel signal chooses between the 20 inputs.
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;



entity mux_66b_20to1 is
  port ( sel : in std_logic_vector(4 downto 0);
         i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : in std_logic_vector(65 downto 0);
         o   : out std_logic_vector(65 downto 0)
         );
end mux_66b_20to1;


architecture behavioral of mux_66b_20to1 is
begin

process(sel,i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19)
begin
case sel is

  when "00000"=> o <= i0;
  when "00001"=> o <= i1;
  when "00010"=> o <= i2;
  when "00011"=> o <= i3;
  when "00100"=> o <= i4;
  when "00101"=> o <= i5;
  when "00110"=> o <= i6;
  when "00111"=> o <= i7;
  when "01000"=> o <= i8;
  when "01001"=> o <= i9;
  when "01010"=> o <= i10;
  when "01011"=> o <= i11;
  when "01100"=> o <= i12;
  when "01101"=> o <= i13;
  when "01110"=> o <= i14;
  when "01111"=> o <= i15;
  when "10000"=> o <= i16;
  when "10001"=> o <= i17;
  when "10010"=> o <= i18;
  when "10011"=> o <= i19;
  when others => o <= i0;

end case;
end process;
end architecture;
