-------------------------------------------------------------------------
-- Crossbar control unit
--
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity ctl_crossbar is
  port ( clk : in std_logic;
         reset : in std_logic;
         data_valid : in std_logic;
         cfg : in std_logic;
         sel : out std_logic_vector(99 downto 0)
         );
end ctl_crossbar;


architecture Behavioral of ctl_crossbar is


  component dual_port_ram_generic
   generic( ram_width : integer := 8);
   port(clk : in  std_logic;
       wr_en : in  std_logic;
       data_in : in  std_logic_vector(ram_width-1 downto 0);
       waddr : in  std_logic_vector(7 downto 0);
       raddr : in  std_logic_vector(7 downto 0);
       port_en_0 : in  std_logic;
       port_en_1 : in  std_logic;
       data_out : out  std_logic_vector(ram_width-1 downto 0)
      );
  end component;



  signal cnt : std_logic_vector(7 downto 0);
  signal raddr : std_logic_vector(7 downto 0);
  signal int_sel : std_logic_vector(99 downto 0);
  signal start : std_logic := '0';
  signal valid_cnt :std_logic_vector(7 downto 0);



begin

-- Memory for selector signal(configurations)
  dual_port_ram : dual_port_ram_generic GENERIC MAP(
          ram_width => 100
          )

  PORT MAP (
          clk => clk,
          wr_en => '0',
          data_in => (others => '0'),
          data_out => int_sel,
          waddr => (others => '0'),
          raddr => raddr,
          port_en_0 => '0',
          port_en_1 => '1'
      );



-- Counter to run through the 80 selector signals.
  process(clk,reset)
  begin
    if(reset='1') then
      --cnt <= (others => '0');
      cnt <= "00000001";
    elsif(rising_edge(clk)) then
      if(data_valid = '1') then
        if(cnt = "01001111") then
          cnt <= (others => '0');
        else
          cnt <= cnt + 1;
        end if;
      end if;
    end if;
  end process;


 -- Config 0 or 1
  raddr <= cnt when cfg = '0' else cnt + 80; -- r_addr for config memory.

  -- Ouput signals
  sel <= int_sel;


end architecture;
