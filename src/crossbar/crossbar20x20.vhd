-------------------------------------------------------------------------
-- 20x20 crossbar
-- 20x 66bit - 20to1 mux added together to form a crossbar.
-- Conrtolled by the ctr_crossbar
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


entity crossbar20x20 is
  port ( i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : in std_logic_vector(65 downto 0);
         sel : in std_logic_vector(99 downto 0);
         o0,o1,o2,o3,o4,o5,o6,o7,o8,o9,o10,o11,o12,o13,o14,o15,o16,o17,o18,o19   : out std_logic_vector(65 downto 0)
         );
end crossbar20x20;


architecture behavioral of crossbar20x20 is

-- component
component mux_66b_20to1
port ( sel : in std_logic_vector(4 downto 0);
       i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19 : in std_logic_vector(65 downto 0);
       o   : out std_logic_vector(65 downto 0)
       );
end component;




begin

  -- mux0: y0 outputs
  mux0 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(99 downto 95),
    o => o0
  );

  mux1 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(94 downto 90),
    o => o1
  );

  mux2 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(89 downto 85),
    o => o2
  );

  mux3 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(84 downto 80),
    o => o3
  );

  mux4 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(79 downto 75),
    o => o4
  );

  mux5 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(74 downto 70),
    o => o5
  );

  mux6 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(69 downto 65),
    o => o6
  );

  mux7 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(64 downto 60),
    o => o7
  );

  mux8 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(59 downto 55),
    o => o8
  );

  mux9 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(54 downto 50),
    o => o9
  );

  mux10 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(49 downto 45),
    o => o10
  );

  mux11 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(44 downto 40),
    o => o11
  );

  mux12 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(39 downto 35),
    o => o12
  );

  mux13 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(34 downto 30),
    o => o13
  );

  mux14 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(29 downto 25),
    o => o14
  );

  mux15 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(24 downto 20),
    o => o15
  );

  mux16 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(19 downto 15),
    o => o16
  );

  mux17 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(14 downto 10),
    o => o17
  );

  mux18 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(9 downto 5),
    o => o18
  );

  mux19 : mux_66b_20to1 PORT MAP(
    i0 => i0,
    i1 => i1,
    i2 => i2,
    i3 => i3,
    i4 => i4,
    i5 => i5,
    i6 => i6,
    i7 => i7,
    i8 => i8,
    i9 => i9,
    i10 => i10,
    i11 => i11,
    i12 => i12,
    i13 => i13,
    i14 => i14,
    i15 => i15,
    i16 => i16,
    i17 => i17,
    i18 => i18,
    i19 => i19,
    sel => sel(4 downto 0),
    o => o19
  );


end architecture;
