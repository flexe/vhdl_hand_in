-------------------------------------------------------------------------
-- PRBS31 generator
-- Anders Jørgensen
-------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity prbs is
  port(
    clk : in std_logic;
    reset : in std_logic;
    start : in std_logic;
    prbs   : out std_logic_vector(1319 downto 0));
end prbs;



architecture behavior of prbs is

  signal lfsr : std_logic_vector(31 downto 1);
  signal int_prbs : std_logic_vector(65 downto 0);

begin

  process (clk, reset,start) is
    variable lfsr_tap : std_ulogic;
  begin
    if rising_edge(clk) then
      if (start = '1') then
        if unsigned(lfsr) /= 0 then
          lfsr_tap := lfsr(31) xor lfsr(28);
          lfsr <= lfsr(30 downto 1) & lfsr_tap;
        end if;
      end if;
    end if;
    if reset = '1' then
      lfsr <= std_logic_vector(to_unsigned(1, 31));  -- Reset assigns 1 to lfsr signal
    end if;
  end process;


  prbs(1319 downto 31) <= (others => '0');
  prbs(30 downto 0) <= lfsr;
  --
  -- int_prbs(65 downto 31) <= (others => '0');
  -- int_prbs(30 downto 0) <= lfsr;
  --
  --
  -- prbs(1319 downto 1254) <= int_prbs;
  -- prbs(1253 downto 1188) <= int_prbs;
  -- prbs(1187 downto 1122) <= int_prbs;
  -- prbs(1121 downto 1056) <= int_prbs;
  -- prbs(1055 downto 990) <= int_prbs;
  -- prbs(989 downto 924) <= int_prbs;
  -- prbs(923 downto 858) <= int_prbs;
  -- prbs(857 downto 792) <= int_prbs;
  -- prbs(791 downto 726) <= int_prbs;
  -- prbs(725 downto 660) <= int_prbs;
  -- prbs(659 downto 594) <= int_prbs;
  -- prbs(593 downto 528) <= int_prbs;
  -- prbs(527 downto 462) <= int_prbs;
  -- prbs(461 downto 396) <= int_prbs;
  -- prbs(395 downto 330) <= int_prbs;
  -- prbs(329 downto 264) <= int_prbs;
  -- prbs(263 downto 198) <= int_prbs;
  -- prbs(197 downto 132) <= int_prbs;
  -- prbs(131 downto 66) <= int_prbs;
  -- prbs(65 downto 0) <= int_prbs;


end;
