library ieee;
library modelsim_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.math_real.all;
use modelsim_lib.util.all;



entity tb_prbs is
end tb_prbs;

architecture behavior of tb_prbs is


  component prbs is
    port(
      clk : in std_logic;
      reset  : in std_logic;
      start  : in std_logic;
      prbs : out std_logic_vector(65 downto 0));
  end component;

signal clk : std_logic;
signal reset : std_logic;
signal int_prbs : std_logic_vector(65 downto 0);
signal int_start : std_logic := '0';


constant CLK_PERIOD : time := 10 ns;


begin

      -- instantiate the unit under test (uut)
      uut : prbs PORT MAP (
              clk => clk,
              reset => reset,
              start => int_start,
              prbs => int_prbs
          );



    clk_process :process
      begin
        clk <= '0';
          wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
          clk <= '1';
          wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
    end process;


    reset_process : process
      begin
        wait for 10 ns;
        reset <= '1';
        wait for 10 ns;
        reset <= '0';
        wait for 10 ns;
        int_start <= '1';
        wait;
      end process;


end;
