-------------------------------------------------------------------------
-- top file for synthesis
-- 1320 input/output are and'ed together and anssigned to pins,
--             ensuring no components disapears under synthesis.
-- Anders Jørgensen
-------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;



entity top_syn is
  port ( clk : in std_logic;
         reset : in std_logic;
         input_and : in std_logic; --AND all 1320 input
         output_and : out std_logic --AND all 1320 output
         );
end top_syn;


architecture behavioral of top_syn is



  component flexe_top is
      port ( clk : in std_logic;
             reset : in std_logic;
             --data_valid : in std_logic;
             data_in : in std_logic_vector(1319 downto 0);
             data_out : out std_logic_vector(1319 downto 0);
             data_valid : in std_logic
             );
end component;

      signal int_clk : std_logic;
      signal int_reset : std_logic;
      signal int_data_in : std_logic_vector(1319 downto 0);
      signal int_data_out : std_logic_vector(1319 downto 0);
      signal int_data_valid : std_logic;
      signal int_input : std_logic;
      signal test : std_logic;

  begin


    utos : flexe_top PORT MAP (
            clk => int_clk,
            reset => int_reset,
            data_valid => '0',
            data_in => int_data_in,
            data_out => int_data_out
        );

output_and <= and_reduce(int_data_out);
int_clk <= clk;
int_reset <= reset;
--int_data_valid <= data_valid;

-- Input assignment
process(input_and)
begin
for i in 0 to 1319 loop
    int_data_in(i) <= input_and;
end loop;
end process;


end;
