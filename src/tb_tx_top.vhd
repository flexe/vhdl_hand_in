-------------------------------------------------------------------------
-- Test bench for flexe mux top file
-- One calendar component
-- data ->TX-> FlexE
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
library modelsim_lib;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.math_real.all;
use modelsim_lib.util.all;



entity tb_tx_top is
end tb_tx_top;

architecture behavior of tb_tx_top is

component flexe_top is
    port ( clk : in std_logic;
           reset : in std_logic;
           --data_valid : in std_logic;
           cfg_stage1 : in std_logic;
           cfg_stage2 : in std_logic;
           cfg_stage3 : in std_logic;
           data_in : in std_logic_vector(1319 downto 0);
           data_out : out std_logic_vector(1319 downto 0);
           new_cfg : out std_logic;
           data_valid : in std_logic
           );
  end component;


   component data_gen is
     port ( clk : in std_logic;
            reset : in std_logic;
            start : in std_logic;
            data_out : out std_logic_vector
            );
   end component;



signal clk : std_logic;
signal reset : std_logic;
signal data_valid : std_logic;
signal data_in : std_logic_vector(1319 downto 0);
signal data_out : std_logic_vector(1319 downto 0);

signal int_data_gen : std_logic_vector(1319 downto 0);
signal int_data_gen2 : std_logic_vector(1319 downto 0);

signal int_cfg_stage1 : std_logic;
signal int_cfg_stage2 : std_logic;
signal int_cfg_stage3 : std_logic;

signal int_cfg_stage1_rx : std_logic;
signal int_cfg_stage2_rx : std_logic;
signal int_cfg_stage3_rx : std_logic;
--signal data_gen : std_logic_vector(1319 downto 0);



  constant CLK_PERIOD : time := 10 ns;



begin



  uut : flexe_top PORT MAP (
          clk => clk,
          reset => reset,
          data_valid => data_valid,
          data_in => int_data_gen,
          cfg_stage1 => int_cfg_stage1,
          cfg_stage2 => int_cfg_stage2,
          cfg_stage3 => int_cfg_stage3,
          data_out => data_out
      );

     data_gen_0 : data_gen PORT MAP(
            clk => clk,
            reset => reset,
            start => data_valid,
            data_out => int_data_gen2
       );


clk_process :process
  begin
    clk <= '0';
      wait for CLK_PERIOD/2;  --for half of clock period clk stays at '0'.
      clk <= '1';
      wait for CLK_PERIOD/2;  --for next half of clock period clk stays at '1'.
end process;


reset_process : process
begin
  --wait for 10 ns;
  reset <= '1';
  wait for 10 ns;
  reset <= '0';
  wait;
end process;


--data_in <= (others => '0');


-- data gen
data_gen_process : process

  begin
    data_valid <= '0';
    wait for 10 ns;
    data_valid <= '1';
    int_data_gen <= conv_std_logic_vector(1,66) & conv_std_logic_vector(2,66) &  conv_std_logic_vector(3,66) & conv_std_logic_vector(4,66) & conv_std_logic_vector(5,66) &
    conv_std_logic_vector(6,66) & conv_std_logic_vector(7,66) & conv_std_logic_vector(8,66) & conv_std_logic_vector(9,66) & conv_std_logic_vector(10,66) &
    conv_std_logic_vector(11,66) & conv_std_logic_vector(12,66) & conv_std_logic_vector(13,66) & conv_std_logic_vector(14,66) & conv_std_logic_vector(15,66) &
    conv_std_logic_vector(16,66) & conv_std_logic_vector(17,66) & conv_std_logic_vector(18,66) & conv_std_logic_vector(19,66) & conv_std_logic_vector(20,66);
    wait for 10 ns;
    int_data_gen <= (others => '0');




    wait;



  end process;




  cfg_process : process

  begin

    int_cfg_stage1 <= '0';
    int_cfg_stage2 <= '0';
    int_cfg_stage3 <= '0';
    wait for 1605 ns;

    --int_cfg_stage1 <= '1';
    --int_cfg_stage2 <= '1';
    wait for 800 ns;
    --int_cfg_stage3 <= '1';
    wait;

  end process;






end;
