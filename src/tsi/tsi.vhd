-------------------------------------------------------------------------
-- TSI top file
-- Single time slot interchange
-- Two DP rams and a tsi control unit
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tsi is
  port(  clk : in std_logic;
         reset : in std_logic;
         data_valid : in std_logic;
         cfg_data : in std_logic_vector(7 downto 0);
         cfg_waddr : in std_logic_vector(7 downto 0);
         cfg_wr_en : in std_logic;
         cfg : in std_logic;
         data_in : in std_logic_vector(65 downto 0);
         rdy : out std_logic;
         new_cfg : out std_logic;
         data_out : out std_logic_vector(65 downto 0)
         );
end tsi;



architecture behavioral of tsi is


-------------------------------------------------------------------------
--                       Components
-------------------------------------------------------------------------

component dual_port_ram_generic
 generic( ram_width : integer := 8);
 port(clk : in  std_logic;
     wr_en : in  std_logic;
     data_in : in  std_logic_vector(ram_width-1 downto 0);
     waddr : in  std_logic_vector(7 downto 0);
     raddr : in  std_logic_vector(7 downto 0);
     port_en_0 : in  std_logic;
     port_en_1 : in  std_logic;
     data_out : out  std_logic_vector(ram_width-1 downto 0)
    );
end component;

component tsi_control
  port( clk : in std_logic;
      reset : in std_logic;
      data_valid : in std_logic;
      cfg_data : in std_logic_vector(7 downto 0);
      cfg_raddr : out std_logic_vector(7 downto 0);
      cfg : in std_logic;
      cfg_port_en : out std_logic;
      data_waddr : out std_logic_vector(7 downto 0);
      data_raddr : out std_logic_vector(7 downto 0);
      data_write_port_en : out std_logic;
      data_read_port_en : out std_logic;
      rdy : out std_logic;
      new_cfg : out std_logic;
      data_wr_en : out std_logic
    );
end component;



-------------------------------------------------------------------------
--                     Internal signals
-------------------------------------------------------------------------
signal int_data_wr_en : std_logic;
signal int_data_waddr : std_logic_vector(7 downto 0);
signal int_data_raddr : std_logic_vector(7 downto 0);
signal int_data_write_port_en : std_logic;
signal int_data_read_port_en : std_logic;


-- Signals from loading a new configuration into r_ptr memory(not in use)
signal int_cfg_data : std_logic_vector(7 downto 0);
signal int_cfg_raddr : std_logic_vector(7 downto 0);
signal int_cfg_port_en : std_logic;



begin


-------------------------------------------------------------------------
--                Dual port ram for data memory
-------------------------------------------------------------------------


dual_port_ram_data : dual_port_ram_generic GENERIC MAP(
        ram_width => 66
        )

PORT MAP (
        clk => clk,
        wr_en => int_data_wr_en,
        data_in => data_in,
        data_out => data_out,
        waddr => int_data_waddr,
        raddr => int_data_raddr,
        port_en_0 => int_data_write_port_en,
        port_en_1 => int_data_read_port_en
    );

-------------------------------------------------------------------------
--                  TSI control unit
-------------------------------------------------------------------------
control_unit : tsi_control PORT MAP (
        clk => clk,
        reset => reset,
        data_valid => data_valid,
        cfg => cfg,
        cfg_data => int_cfg_data,
        cfg_port_en => int_cfg_port_en,
        cfg_raddr => int_cfg_raddr,
        data_waddr => int_data_waddr,
        data_raddr => int_data_raddr,
        data_write_port_en => int_data_write_port_en,
        data_read_port_en => int_data_read_port_en,
        rdy => rdy,
        new_cfg => new_cfg,
        data_wr_en => int_data_wr_en
);


-------------------------------------------------------------------------
--                Dual port ram for r_ptr memory
-------------------------------------------------------------------------

dual_port_ram_rptr : dual_port_ram_generic GENERIC MAP(
        ram_width => 8
        )
PORT MAP (
        clk => clk,
        wr_en => cfg_wr_en,
        data_in => cfg_data,
        data_out => int_cfg_data,
        waddr => cfg_waddr,
        raddr => int_cfg_raddr,
        port_en_0 => cfg_wr_en,
        port_en_1 => int_cfg_port_en
        );




end;
