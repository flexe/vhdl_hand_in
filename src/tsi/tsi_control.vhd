-------------------------------------------------------------------------
-- TSI control unit
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;


entity tsi_control is
 port( clk : in std_logic;
       reset : in std_logic;
       data_valid : in std_logic;
       cfg_data : in std_logic_vector(7 downto 0);
       cfg : in std_logic;
       cfg_raddr : out std_logic_vector(7 downto 0);
       cfg_port_en : out std_logic;
       data_waddr : out std_logic_vector(7 downto 0);
       data_raddr : out std_logic_vector(7 downto 0);
       data_write_port_en : out std_logic;
       data_read_port_en : out std_logic;
       rdy : out std_logic;
       new_cfg : out std_logic;
       data_wr_en : out std_logic);
end entity;

architecture behavioral of tsi_control is


-------------------------------------------------------------------------
--                       Internal signals
-------------------------------------------------------------------------

  signal waddr_count : std_logic_vector(7 downto 0);
  signal raddr_count : std_logic_vector(7 downto 0);

  signal int_raddr : std_logic_vector(7 downto 0);
  signal ram_block_part : std_logic := '0'; -- memory paging

  signal begin_read : std_logic := '0'; -- First 80 memory entries are ready = start reading

  signal int_rdy : std_logic := '0'; --Set to 1 when next stage should begin
  signal int_new_cfg : std_logic := '0';



begin



  --int_rdy <= '1' when begin_read = '1' else '0'; --next stage should start when, this tsi starts to readout data from data memory.
  int_new_cfg <= '1' when raddr_count = "01001111" else '0'; -- When the



-- waddr counter process
-- Goes from 0 to 159
-- Determines the w_ptr
  waddr_cnt : process(clk,reset)
  begin
    if(reset='1') then
      waddr_count <= (others => '0');
    elsif(rising_edge(clk)) then
      if(data_valid='1') then
        if(waddr_count = "10011111") then --counter goes from 0..159
          waddr_count <= (others => '0');
        else
          waddr_count <= waddr_count + 1;
        end if;
        if(waddr_count = "01001101") then
          begin_read <= '1';
          --int_rdy <='1';
        elsif(waddr_count = "01001111") then
          int_rdy <= '1';
        end if;
      end if;
    end if;
  end process;


  data_valid_out : process(clk, reset, begin_read)
  begin
    if(reset='1') then
      --int_rdy <= '0';
      data_read_port_en <= '0';
    elsif(rising_edge(clk)) then
      if (begin_read='1') then
        --int_rdy <= '1';
        data_read_port_en <='1';
      end if;
    end if;
  end process;


  -- raddr_count is used to run through the r_ptr memory.
  -- goes from 0 to 79
  -- Handles paging, by shifting memory block every 80th clock.
  raddr_cnt : process(clk, reset)
  begin
    if(reset='1') then
      raddr_count <= (others => '0');
    elsif (rising_edge(clk)) then
      if(raddr_count = "01001111") then
        raddr_count <= (others => '0');
        ram_block_part <= NOT ram_block_part; -- paging
      else
        if(begin_read='1') then
          raddr_count <= raddr_count + 1;

        end if;
      end if;
    end if;
  end process;


-------------------------------------------------------------------------
--                       Output signals
-------------------------------------------------------------------------
data_write_port_en <= data_valid; -- enable data in port
data_wr_en <= data_valid; -- enable write for data in port
data_waddr <= waddr_count; -- w_ptr
data_raddr <= cfg_data + 80 when ram_block_part='1' else cfg_data; -- instruction from r_ptr memory, and pagin logic
--data_read_port_en <= '1' when begin_read='1' else '0'; -- when to start reading from data memory
--data_read_port_en <= '1';
cfg_raddr <= raddr_count + 80 when cfg='1' else raddr_count; -- Whci config to read from r_ptr memory(0 or 1)
cfg_port_en <= '1'; --when begin_read='1' else '0'; -- enable config port, always on,
rdy <= int_rdy; -- ready signal to next stage
new_cfg <= int_new_cfg; -- pulse to indicate when config swtich can be done


end architecture;
