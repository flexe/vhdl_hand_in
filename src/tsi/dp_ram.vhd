-------------------------------------------------------------------------
-- Generic Dual Port memory
-- Two ports, one for write and one for read.
-- Anders Jørgensen
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_unsigned.all;


--dual_port_ram_generic
--dp_ram
entity dual_port_ram_generic is
  generic ( ram_width : integer := 8); -- Choose entry size,(data_mem = 66, r_ptr_mem = 8, crossbar_ctl = 100)
port(   clk: in std_logic;
        wr_en : in std_logic;
        data_in : in std_logic_vector(ram_width-1 downto 0);
        waddr : in std_logic_vector(7 downto 0);
        raddr : in std_logic_vector(7 downto 0);
        --waddr	: in natural range 0 to 159;
        --raddr	: in natural range 0 to 159;
        port_en_0 : in std_logic;
        port_en_1 : in std_logic;
        data_out : out std_logic_vector(ram_width-1 downto 0)
    );
end dual_port_ram_generic;

architecture rtl of dual_port_ram_generic is

-- RAM block
--type ram_type is array(0 to 159) of std_logic_vector(ram_width-1 downto 0); -- Depth = 160
--signal ram : ram_type := (others => (others => '0'));


-- Build a 2-D array type for the RAM
subtype word_t is std_logic_vector(ram_width-1 downto 0);
type memory_t is array(159 downto 0) of word_t;

-- Declare the RAM
shared variable ram : memory_t;

signal waddr_int : integer;
signal raddr_int : integer;





begin

waddr_int <= to_integer(unsigned(waddr));
raddr_int <= to_integer(unsigned(raddr));
-- Write operation to port 0
process(clk)
begin
    if(rising_edge(clk)) then
        --if(port_en_0 = '1') then
            if(wr_en = '1') then
                ram(waddr_int) := data_in;
            end if;
        --end if;
    end if;
end process;

process(clk)
begin
  if(rising_edge(clk)) then
    if (port_en_1 = '1') then
      data_out <= ram(raddr_int);
    else
      data_out <= (others => 'Z');
    end if;
  end if;
end process;

-- Readout from port 1
--data_out <= ram(raddr) when (port_en_1 = '1') else
  --          (others => 'Z');
--data_out <= ram(raddr_int) when (port_en_1 = '1' ) else
        --      (others => 'Z');


end rtl;
