onerror {resume}
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(1319 downto 990)} PHY0
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(989 downto 660)} PHY1
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(659 downto 330)} PHY2
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(329 downto 0)} PHY3
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(659 downto 594)} PHY2_LANE0
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(659 downto 594)} PHY2_LANE_0_15
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(593 downto 528)} PHY2_LANE_1_6_11_16
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(527 downto 462)} PHY2_LANE_2_7_12_17
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(461 downto 396)} PHY2_LANE_3_8_13_18
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(395 downto 330)} PHY2_LANE_4_9_14_19
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_in(1319 downto 1254)} CH0_0
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_in(1253 downto 1189)} CH0_1
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_in(1253 downto 1188)} CH0_1001
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(1319 downto 1254)} PHY0_LANE_0_5_10_15
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(1253 downto 1188)} PHY0_LANE_1_6_11_16
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(1187 downto 1122)} PHY0_LANE_2_7_12_17
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(1121 downto 1056)} PHY0_LANE_2_8_13_18
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(1055 downto 990)} PHY0_LANE_4_9_14_19
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(989 downto 924)} PHY1_LANE_0_5_10_15
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(923 downto 858)} PHY1_LANE_1_6_11_16
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(857 downto 792)} PHY1_LANE_2_7_12_17
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(791 downto 726)} PHY1_LANE_3_8_13_18
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(725 downto 660)} PHY1_LANE_4_9_14_19
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(329 downto 264)} PHY3_LANE_0_5_10_15
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(263 downto 198)} PHY3_LANE_1_6_11_16
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(197 downto 132)} PHY3_LANE_2_7_12_17
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(131 downto 66)} PHY3_LANE_3_8_13_18
quietly virtual signal -install /tb_tx_top { /tb_tx_top/data_out(65 downto 0)} PHY3_LANE_4_9_14_19
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(1319 downto 1254)} B0
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(1253 downto 1188)} B1
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(1187 downto 1122)} B2
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(65 downto 0)} B19
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(131 downto 66)} B18
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(197 downto 132)} B17
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(263 downto 198)} B16
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(329 downto 264)} B15
quietly virtual signal -install /tb_tx_top/uut { /tb_tx_top/uut/data_in(395 downto 330)} B14
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_tx_top/clk
add wave -noupdate /tb_tx_top/reset
add wave -noupdate /tb_tx_top/data_valid
add wave -noupdate -radix unsigned /tb_tx_top/uut/B1
add wave -noupdate -radix unsigned -childformat {{/tb_tx_top/uut/B2(1187) -radix unsigned} {/tb_tx_top/uut/B2(1186) -radix unsigned} {/tb_tx_top/uut/B2(1185) -radix unsigned} {/tb_tx_top/uut/B2(1184) -radix unsigned} {/tb_tx_top/uut/B2(1183) -radix unsigned} {/tb_tx_top/uut/B2(1182) -radix unsigned} {/tb_tx_top/uut/B2(1181) -radix unsigned} {/tb_tx_top/uut/B2(1180) -radix unsigned} {/tb_tx_top/uut/B2(1179) -radix unsigned} {/tb_tx_top/uut/B2(1178) -radix unsigned} {/tb_tx_top/uut/B2(1177) -radix unsigned} {/tb_tx_top/uut/B2(1176) -radix unsigned} {/tb_tx_top/uut/B2(1175) -radix unsigned} {/tb_tx_top/uut/B2(1174) -radix unsigned} {/tb_tx_top/uut/B2(1173) -radix unsigned} {/tb_tx_top/uut/B2(1172) -radix unsigned} {/tb_tx_top/uut/B2(1171) -radix unsigned} {/tb_tx_top/uut/B2(1170) -radix unsigned} {/tb_tx_top/uut/B2(1169) -radix unsigned} {/tb_tx_top/uut/B2(1168) -radix unsigned} {/tb_tx_top/uut/B2(1167) -radix unsigned} {/tb_tx_top/uut/B2(1166) -radix unsigned} {/tb_tx_top/uut/B2(1165) -radix unsigned} {/tb_tx_top/uut/B2(1164) -radix unsigned} {/tb_tx_top/uut/B2(1163) -radix unsigned} {/tb_tx_top/uut/B2(1162) -radix unsigned} {/tb_tx_top/uut/B2(1161) -radix unsigned} {/tb_tx_top/uut/B2(1160) -radix unsigned} {/tb_tx_top/uut/B2(1159) -radix unsigned} {/tb_tx_top/uut/B2(1158) -radix unsigned} {/tb_tx_top/uut/B2(1157) -radix unsigned} {/tb_tx_top/uut/B2(1156) -radix unsigned} {/tb_tx_top/uut/B2(1155) -radix unsigned} {/tb_tx_top/uut/B2(1154) -radix unsigned} {/tb_tx_top/uut/B2(1153) -radix unsigned} {/tb_tx_top/uut/B2(1152) -radix unsigned} {/tb_tx_top/uut/B2(1151) -radix unsigned} {/tb_tx_top/uut/B2(1150) -radix unsigned} {/tb_tx_top/uut/B2(1149) -radix unsigned} {/tb_tx_top/uut/B2(1148) -radix unsigned} {/tb_tx_top/uut/B2(1147) -radix unsigned} {/tb_tx_top/uut/B2(1146) -radix unsigned} {/tb_tx_top/uut/B2(1145) -radix unsigned} {/tb_tx_top/uut/B2(1144) -radix unsigned} {/tb_tx_top/uut/B2(1143) -radix unsigned} {/tb_tx_top/uut/B2(1142) -radix unsigned} {/tb_tx_top/uut/B2(1141) -radix unsigned} {/tb_tx_top/uut/B2(1140) -radix unsigned} {/tb_tx_top/uut/B2(1139) -radix unsigned} {/tb_tx_top/uut/B2(1138) -radix unsigned} {/tb_tx_top/uut/B2(1137) -radix unsigned} {/tb_tx_top/uut/B2(1136) -radix unsigned} {/tb_tx_top/uut/B2(1135) -radix unsigned} {/tb_tx_top/uut/B2(1134) -radix unsigned} {/tb_tx_top/uut/B2(1133) -radix unsigned} {/tb_tx_top/uut/B2(1132) -radix unsigned} {/tb_tx_top/uut/B2(1131) -radix unsigned} {/tb_tx_top/uut/B2(1130) -radix unsigned} {/tb_tx_top/uut/B2(1129) -radix unsigned} {/tb_tx_top/uut/B2(1128) -radix unsigned} {/tb_tx_top/uut/B2(1127) -radix unsigned} {/tb_tx_top/uut/B2(1126) -radix unsigned} {/tb_tx_top/uut/B2(1125) -radix unsigned} {/tb_tx_top/uut/B2(1124) -radix unsigned} {/tb_tx_top/uut/B2(1123) -radix unsigned} {/tb_tx_top/uut/B2(1122) -radix unsigned}} -subitemconfig {/tb_tx_top/uut/data_in(1187) {-radix unsigned} /tb_tx_top/uut/data_in(1186) {-radix unsigned} /tb_tx_top/uut/data_in(1185) {-radix unsigned} /tb_tx_top/uut/data_in(1184) {-radix unsigned} /tb_tx_top/uut/data_in(1183) {-radix unsigned} /tb_tx_top/uut/data_in(1182) {-radix unsigned} /tb_tx_top/uut/data_in(1181) {-radix unsigned} /tb_tx_top/uut/data_in(1180) {-radix unsigned} /tb_tx_top/uut/data_in(1179) {-radix unsigned} /tb_tx_top/uut/data_in(1178) {-radix unsigned} /tb_tx_top/uut/data_in(1177) {-radix unsigned} /tb_tx_top/uut/data_in(1176) {-radix unsigned} /tb_tx_top/uut/data_in(1175) {-radix unsigned} /tb_tx_top/uut/data_in(1174) {-radix unsigned} /tb_tx_top/uut/data_in(1173) {-radix unsigned} /tb_tx_top/uut/data_in(1172) {-radix unsigned} /tb_tx_top/uut/data_in(1171) {-radix unsigned} /tb_tx_top/uut/data_in(1170) {-radix unsigned} /tb_tx_top/uut/data_in(1169) {-radix unsigned} /tb_tx_top/uut/data_in(1168) {-radix unsigned} /tb_tx_top/uut/data_in(1167) {-radix unsigned} /tb_tx_top/uut/data_in(1166) {-radix unsigned} /tb_tx_top/uut/data_in(1165) {-radix unsigned} /tb_tx_top/uut/data_in(1164) {-radix unsigned} /tb_tx_top/uut/data_in(1163) {-radix unsigned} /tb_tx_top/uut/data_in(1162) {-radix unsigned} /tb_tx_top/uut/data_in(1161) {-radix unsigned} /tb_tx_top/uut/data_in(1160) {-radix unsigned} /tb_tx_top/uut/data_in(1159) {-radix unsigned} /tb_tx_top/uut/data_in(1158) {-radix unsigned} /tb_tx_top/uut/data_in(1157) {-radix unsigned} /tb_tx_top/uut/data_in(1156) {-radix unsigned} /tb_tx_top/uut/data_in(1155) {-radix unsigned} /tb_tx_top/uut/data_in(1154) {-radix unsigned} /tb_tx_top/uut/data_in(1153) {-radix unsigned} /tb_tx_top/uut/data_in(1152) {-radix unsigned} /tb_tx_top/uut/data_in(1151) {-radix unsigned} /tb_tx_top/uut/data_in(1150) {-radix unsigned} /tb_tx_top/uut/data_in(1149) {-radix unsigned} /tb_tx_top/uut/data_in(1148) {-radix unsigned} /tb_tx_top/uut/data_in(1147) {-radix unsigned} /tb_tx_top/uut/data_in(1146) {-radix unsigned} /tb_tx_top/uut/data_in(1145) {-radix unsigned} /tb_tx_top/uut/data_in(1144) {-radix unsigned} /tb_tx_top/uut/data_in(1143) {-radix unsigned} /tb_tx_top/uut/data_in(1142) {-radix unsigned} /tb_tx_top/uut/data_in(1141) {-radix unsigned} /tb_tx_top/uut/data_in(1140) {-radix unsigned} /tb_tx_top/uut/data_in(1139) {-radix unsigned} /tb_tx_top/uut/data_in(1138) {-radix unsigned} /tb_tx_top/uut/data_in(1137) {-radix unsigned} /tb_tx_top/uut/data_in(1136) {-radix unsigned} /tb_tx_top/uut/data_in(1135) {-radix unsigned} /tb_tx_top/uut/data_in(1134) {-radix unsigned} /tb_tx_top/uut/data_in(1133) {-radix unsigned} /tb_tx_top/uut/data_in(1132) {-radix unsigned} /tb_tx_top/uut/data_in(1131) {-radix unsigned} /tb_tx_top/uut/data_in(1130) {-radix unsigned} /tb_tx_top/uut/data_in(1129) {-radix unsigned} /tb_tx_top/uut/data_in(1128) {-radix unsigned} /tb_tx_top/uut/data_in(1127) {-radix unsigned} /tb_tx_top/uut/data_in(1126) {-radix unsigned} /tb_tx_top/uut/data_in(1125) {-radix unsigned} /tb_tx_top/uut/data_in(1124) {-radix unsigned} /tb_tx_top/uut/data_in(1123) {-radix unsigned} /tb_tx_top/uut/data_in(1122) {-radix unsigned}} /tb_tx_top/uut/B2
add wave -noupdate -radix unsigned -childformat {{/tb_tx_top/uut/B0(1319) -radix unsigned} {/tb_tx_top/uut/B0(1318) -radix unsigned} {/tb_tx_top/uut/B0(1317) -radix unsigned} {/tb_tx_top/uut/B0(1316) -radix unsigned} {/tb_tx_top/uut/B0(1315) -radix unsigned} {/tb_tx_top/uut/B0(1314) -radix unsigned} {/tb_tx_top/uut/B0(1313) -radix unsigned} {/tb_tx_top/uut/B0(1312) -radix unsigned} {/tb_tx_top/uut/B0(1311) -radix unsigned} {/tb_tx_top/uut/B0(1310) -radix unsigned} {/tb_tx_top/uut/B0(1309) -radix unsigned} {/tb_tx_top/uut/B0(1308) -radix unsigned} {/tb_tx_top/uut/B0(1307) -radix unsigned} {/tb_tx_top/uut/B0(1306) -radix unsigned} {/tb_tx_top/uut/B0(1305) -radix unsigned} {/tb_tx_top/uut/B0(1304) -radix unsigned} {/tb_tx_top/uut/B0(1303) -radix unsigned} {/tb_tx_top/uut/B0(1302) -radix unsigned} {/tb_tx_top/uut/B0(1301) -radix unsigned} {/tb_tx_top/uut/B0(1300) -radix unsigned} {/tb_tx_top/uut/B0(1299) -radix unsigned} {/tb_tx_top/uut/B0(1298) -radix unsigned} {/tb_tx_top/uut/B0(1297) -radix unsigned} {/tb_tx_top/uut/B0(1296) -radix unsigned} {/tb_tx_top/uut/B0(1295) -radix unsigned} {/tb_tx_top/uut/B0(1294) -radix unsigned} {/tb_tx_top/uut/B0(1293) -radix unsigned} {/tb_tx_top/uut/B0(1292) -radix unsigned} {/tb_tx_top/uut/B0(1291) -radix unsigned} {/tb_tx_top/uut/B0(1290) -radix unsigned} {/tb_tx_top/uut/B0(1289) -radix unsigned} {/tb_tx_top/uut/B0(1288) -radix unsigned} {/tb_tx_top/uut/B0(1287) -radix unsigned} {/tb_tx_top/uut/B0(1286) -radix unsigned} {/tb_tx_top/uut/B0(1285) -radix unsigned} {/tb_tx_top/uut/B0(1284) -radix unsigned} {/tb_tx_top/uut/B0(1283) -radix unsigned} {/tb_tx_top/uut/B0(1282) -radix unsigned} {/tb_tx_top/uut/B0(1281) -radix unsigned} {/tb_tx_top/uut/B0(1280) -radix unsigned} {/tb_tx_top/uut/B0(1279) -radix unsigned} {/tb_tx_top/uut/B0(1278) -radix unsigned} {/tb_tx_top/uut/B0(1277) -radix unsigned} {/tb_tx_top/uut/B0(1276) -radix unsigned} {/tb_tx_top/uut/B0(1275) -radix unsigned} {/tb_tx_top/uut/B0(1274) -radix unsigned} {/tb_tx_top/uut/B0(1273) -radix unsigned} {/tb_tx_top/uut/B0(1272) -radix unsigned} {/tb_tx_top/uut/B0(1271) -radix unsigned} {/tb_tx_top/uut/B0(1270) -radix unsigned} {/tb_tx_top/uut/B0(1269) -radix unsigned} {/tb_tx_top/uut/B0(1268) -radix unsigned} {/tb_tx_top/uut/B0(1267) -radix unsigned} {/tb_tx_top/uut/B0(1266) -radix unsigned} {/tb_tx_top/uut/B0(1265) -radix unsigned} {/tb_tx_top/uut/B0(1264) -radix unsigned} {/tb_tx_top/uut/B0(1263) -radix unsigned} {/tb_tx_top/uut/B0(1262) -radix unsigned} {/tb_tx_top/uut/B0(1261) -radix unsigned} {/tb_tx_top/uut/B0(1260) -radix unsigned} {/tb_tx_top/uut/B0(1259) -radix unsigned} {/tb_tx_top/uut/B0(1258) -radix unsigned} {/tb_tx_top/uut/B0(1257) -radix unsigned} {/tb_tx_top/uut/B0(1256) -radix unsigned} {/tb_tx_top/uut/B0(1255) -radix unsigned} {/tb_tx_top/uut/B0(1254) -radix unsigned}} -subitemconfig {/tb_tx_top/uut/data_in(1319) {-radix unsigned} /tb_tx_top/uut/data_in(1318) {-radix unsigned} /tb_tx_top/uut/data_in(1317) {-radix unsigned} /tb_tx_top/uut/data_in(1316) {-radix unsigned} /tb_tx_top/uut/data_in(1315) {-radix unsigned} /tb_tx_top/uut/data_in(1314) {-radix unsigned} /tb_tx_top/uut/data_in(1313) {-radix unsigned} /tb_tx_top/uut/data_in(1312) {-radix unsigned} /tb_tx_top/uut/data_in(1311) {-radix unsigned} /tb_tx_top/uut/data_in(1310) {-radix unsigned} /tb_tx_top/uut/data_in(1309) {-radix unsigned} /tb_tx_top/uut/data_in(1308) {-radix unsigned} /tb_tx_top/uut/data_in(1307) {-radix unsigned} /tb_tx_top/uut/data_in(1306) {-radix unsigned} /tb_tx_top/uut/data_in(1305) {-radix unsigned} /tb_tx_top/uut/data_in(1304) {-radix unsigned} /tb_tx_top/uut/data_in(1303) {-radix unsigned} /tb_tx_top/uut/data_in(1302) {-radix unsigned} /tb_tx_top/uut/data_in(1301) {-radix unsigned} /tb_tx_top/uut/data_in(1300) {-radix unsigned} /tb_tx_top/uut/data_in(1299) {-radix unsigned} /tb_tx_top/uut/data_in(1298) {-radix unsigned} /tb_tx_top/uut/data_in(1297) {-radix unsigned} /tb_tx_top/uut/data_in(1296) {-radix unsigned} /tb_tx_top/uut/data_in(1295) {-radix unsigned} /tb_tx_top/uut/data_in(1294) {-radix unsigned} /tb_tx_top/uut/data_in(1293) {-radix unsigned} /tb_tx_top/uut/data_in(1292) {-radix unsigned} /tb_tx_top/uut/data_in(1291) {-radix unsigned} /tb_tx_top/uut/data_in(1290) {-radix unsigned} /tb_tx_top/uut/data_in(1289) {-radix unsigned} /tb_tx_top/uut/data_in(1288) {-radix unsigned} /tb_tx_top/uut/data_in(1287) {-radix unsigned} /tb_tx_top/uut/data_in(1286) {-radix unsigned} /tb_tx_top/uut/data_in(1285) {-radix unsigned} /tb_tx_top/uut/data_in(1284) {-radix unsigned} /tb_tx_top/uut/data_in(1283) {-radix unsigned} /tb_tx_top/uut/data_in(1282) {-radix unsigned} /tb_tx_top/uut/data_in(1281) {-radix unsigned} /tb_tx_top/uut/data_in(1280) {-radix unsigned} /tb_tx_top/uut/data_in(1279) {-radix unsigned} /tb_tx_top/uut/data_in(1278) {-radix unsigned} /tb_tx_top/uut/data_in(1277) {-radix unsigned} /tb_tx_top/uut/data_in(1276) {-radix unsigned} /tb_tx_top/uut/data_in(1275) {-radix unsigned} /tb_tx_top/uut/data_in(1274) {-radix unsigned} /tb_tx_top/uut/data_in(1273) {-radix unsigned} /tb_tx_top/uut/data_in(1272) {-radix unsigned} /tb_tx_top/uut/data_in(1271) {-radix unsigned} /tb_tx_top/uut/data_in(1270) {-radix unsigned} /tb_tx_top/uut/data_in(1269) {-radix unsigned} /tb_tx_top/uut/data_in(1268) {-radix unsigned} /tb_tx_top/uut/data_in(1267) {-radix unsigned} /tb_tx_top/uut/data_in(1266) {-radix unsigned} /tb_tx_top/uut/data_in(1265) {-radix unsigned} /tb_tx_top/uut/data_in(1264) {-radix unsigned} /tb_tx_top/uut/data_in(1263) {-radix unsigned} /tb_tx_top/uut/data_in(1262) {-radix unsigned} /tb_tx_top/uut/data_in(1261) {-radix unsigned} /tb_tx_top/uut/data_in(1260) {-radix unsigned} /tb_tx_top/uut/data_in(1259) {-radix unsigned} /tb_tx_top/uut/data_in(1258) {-radix unsigned} /tb_tx_top/uut/data_in(1257) {-radix unsigned} /tb_tx_top/uut/data_in(1256) {-radix unsigned} /tb_tx_top/uut/data_in(1255) {-radix unsigned} /tb_tx_top/uut/data_in(1254) {-radix unsigned}} /tb_tx_top/uut/B0
add wave -noupdate -radix unsigned /tb_tx_top/uut/B15
add wave -noupdate -radix unsigned /tb_tx_top/uut/B16
add wave -noupdate -radix unsigned /tb_tx_top/uut/B17
add wave -noupdate -radix unsigned /tb_tx_top/uut/B18
add wave -noupdate -radix unsigned /tb_tx_top/uut/B19
add wave -noupdate -group PHY0 -radix unsigned /tb_tx_top/PHY0_LANE_0_5_10_15
add wave -noupdate -group PHY0 -radix unsigned /tb_tx_top/PHY0_LANE_1_6_11_16
add wave -noupdate -group PHY0 -radix unsigned /tb_tx_top/PHY0_LANE_2_7_12_17
add wave -noupdate -group PHY0 -radix unsigned /tb_tx_top/PHY0_LANE_2_8_13_18
add wave -noupdate -group PHY0 -radix unsigned /tb_tx_top/PHY0_LANE_4_9_14_19
add wave -noupdate -group PHY1 -radix unsigned /tb_tx_top/PHY1_LANE_0_5_10_15
add wave -noupdate -group PHY1 -radix unsigned /tb_tx_top/PHY1_LANE_1_6_11_16
add wave -noupdate -group PHY1 -radix unsigned /tb_tx_top/PHY1_LANE_2_7_12_17
add wave -noupdate -group PHY1 -radix unsigned /tb_tx_top/PHY1_LANE_3_8_13_18
add wave -noupdate -group PHY1 -radix unsigned /tb_tx_top/PHY1_LANE_4_9_14_19
add wave -noupdate -expand -group PHY2 -label /tb_tx_top/PHY2_LANE_0_5_10_15 -radix unsigned -childformat {{/tb_tx_top/PHY2_LANE_0_15(659) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(658) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(657) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(656) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(655) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(654) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(653) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(652) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(651) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(650) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(649) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(648) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(647) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(646) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(645) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(644) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(643) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(642) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(641) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(640) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(639) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(638) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(637) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(636) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(635) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(634) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(633) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(632) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(631) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(630) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(629) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(628) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(627) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(626) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(625) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(624) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(623) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(622) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(621) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(620) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(619) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(618) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(617) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(616) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(615) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(614) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(613) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(612) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(611) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(610) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(609) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(608) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(607) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(606) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(605) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(604) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(603) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(602) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(601) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(600) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(599) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(598) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(597) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(596) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(595) -radix unsigned} {/tb_tx_top/PHY2_LANE_0_15(594) -radix unsigned}} -subitemconfig {/tb_tx_top/data_out(659) {-radix unsigned} /tb_tx_top/data_out(658) {-radix unsigned} /tb_tx_top/data_out(657) {-radix unsigned} /tb_tx_top/data_out(656) {-radix unsigned} /tb_tx_top/data_out(655) {-radix unsigned} /tb_tx_top/data_out(654) {-radix unsigned} /tb_tx_top/data_out(653) {-radix unsigned} /tb_tx_top/data_out(652) {-radix unsigned} /tb_tx_top/data_out(651) {-radix unsigned} /tb_tx_top/data_out(650) {-radix unsigned} /tb_tx_top/data_out(649) {-radix unsigned} /tb_tx_top/data_out(648) {-radix unsigned} /tb_tx_top/data_out(647) {-radix unsigned} /tb_tx_top/data_out(646) {-radix unsigned} /tb_tx_top/data_out(645) {-radix unsigned} /tb_tx_top/data_out(644) {-radix unsigned} /tb_tx_top/data_out(643) {-radix unsigned} /tb_tx_top/data_out(642) {-radix unsigned} /tb_tx_top/data_out(641) {-radix unsigned} /tb_tx_top/data_out(640) {-radix unsigned} /tb_tx_top/data_out(639) {-radix unsigned} /tb_tx_top/data_out(638) {-radix unsigned} /tb_tx_top/data_out(637) {-radix unsigned} /tb_tx_top/data_out(636) {-radix unsigned} /tb_tx_top/data_out(635) {-radix unsigned} /tb_tx_top/data_out(634) {-radix unsigned} /tb_tx_top/data_out(633) {-radix unsigned} /tb_tx_top/data_out(632) {-radix unsigned} /tb_tx_top/data_out(631) {-radix unsigned} /tb_tx_top/data_out(630) {-radix unsigned} /tb_tx_top/data_out(629) {-radix unsigned} /tb_tx_top/data_out(628) {-radix unsigned} /tb_tx_top/data_out(627) {-radix unsigned} /tb_tx_top/data_out(626) {-radix unsigned} /tb_tx_top/data_out(625) {-radix unsigned} /tb_tx_top/data_out(624) {-radix unsigned} /tb_tx_top/data_out(623) {-radix unsigned} /tb_tx_top/data_out(622) {-radix unsigned} /tb_tx_top/data_out(621) {-radix unsigned} /tb_tx_top/data_out(620) {-radix unsigned} /tb_tx_top/data_out(619) {-radix unsigned} /tb_tx_top/data_out(618) {-radix unsigned} /tb_tx_top/data_out(617) {-radix unsigned} /tb_tx_top/data_out(616) {-radix unsigned} /tb_tx_top/data_out(615) {-radix unsigned} /tb_tx_top/data_out(614) {-radix unsigned} /tb_tx_top/data_out(613) {-radix unsigned} /tb_tx_top/data_out(612) {-radix unsigned} /tb_tx_top/data_out(611) {-radix unsigned} /tb_tx_top/data_out(610) {-radix unsigned} /tb_tx_top/data_out(609) {-radix unsigned} /tb_tx_top/data_out(608) {-radix unsigned} /tb_tx_top/data_out(607) {-radix unsigned} /tb_tx_top/data_out(606) {-radix unsigned} /tb_tx_top/data_out(605) {-radix unsigned} /tb_tx_top/data_out(604) {-radix unsigned} /tb_tx_top/data_out(603) {-radix unsigned} /tb_tx_top/data_out(602) {-radix unsigned} /tb_tx_top/data_out(601) {-radix unsigned} /tb_tx_top/data_out(600) {-radix unsigned} /tb_tx_top/data_out(599) {-radix unsigned} /tb_tx_top/data_out(598) {-radix unsigned} /tb_tx_top/data_out(597) {-radix unsigned} /tb_tx_top/data_out(596) {-radix unsigned} /tb_tx_top/data_out(595) {-radix unsigned} /tb_tx_top/data_out(594) {-radix unsigned}} /tb_tx_top/PHY2_LANE_0_15
add wave -noupdate -expand -group PHY2 -radix unsigned /tb_tx_top/PHY2_LANE_1_6_11_16
add wave -noupdate -expand -group PHY2 -radix unsigned /tb_tx_top/PHY2_LANE_2_7_12_17
add wave -noupdate -expand -group PHY2 -radix unsigned /tb_tx_top/PHY2_LANE_3_8_13_18
add wave -noupdate -expand -group PHY2 -radix unsigned /tb_tx_top/PHY2_LANE_4_9_14_19
add wave -noupdate -group PHY3 -radix unsigned /tb_tx_top/PHY3_LANE_0_5_10_15
add wave -noupdate -group PHY3 -radix unsigned /tb_tx_top/PHY3_LANE_1_6_11_16
add wave -noupdate -group PHY3 -radix unsigned /tb_tx_top/PHY3_LANE_2_7_12_17
add wave -noupdate -group PHY3 -radix unsigned /tb_tx_top/PHY3_LANE_3_8_13_18
add wave -noupdate -group PHY3 -radix unsigned /tb_tx_top/PHY3_LANE_4_9_14_19
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/clk
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/reset
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/data_valid
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/cfg_data
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/cfg_waddr
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/cfg_wr_en
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/cfg
add wave -noupdate -expand -group tsi0 -radix unsigned /tb_tx_top/uut/tsi0/data_in
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/rdy
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/new_cfg
add wave -noupdate -expand -group tsi0 -radix unsigned /tb_tx_top/uut/tsi0/data_out
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/int_data_wr_en
add wave -noupdate -expand -group tsi0 -radix unsigned /tb_tx_top/uut/tsi0/int_data_waddr
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/int_data_raddr
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/int_data_write_port_en
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/int_data_read_port_en
add wave -noupdate -expand -group tsi0 -radix unsigned /tb_tx_top/uut/tsi0/int_cfg_data
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/int_cfg_raddr
add wave -noupdate -expand -group tsi0 /tb_tx_top/uut/tsi0/int_cfg_port_en
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i0
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i1
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i2
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i3
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i4
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i5
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i6
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i7
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i8
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i9
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i10
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i11
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i12
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i13
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i14
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i15
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i16
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i17
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i18
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/i19
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/sel
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o0
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o1
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o2
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o3
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o4
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o5
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o6
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o7
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o8
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o9
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o10
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o11
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o12
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o13
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o14
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o15
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o16
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o17
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o18
add wave -noupdate -expand -group crossbar -radix unsigned /tb_tx_top/uut/crossbar/o19
add wave -noupdate -expand -group crossbar /tb_tx_top/uut/ctl_crossbar0/int_sel
add wave -noupdate /tb_tx_top/uut/int_data_valid_stage1
add wave -noupdate /tb_tx_top/uut/int_data_valid_stage2
add wave -noupdate /tb_tx_top/uut/int_data_valid_stage3
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/clk
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/reset
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/data_valid
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/cfg_data
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/cfg_waddr
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/cfg_wr_en
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/cfg
add wave -noupdate -expand -group tsi24 -radix unsigned /tb_tx_top/uut/tsi24/data_in
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/rdy
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/new_cfg
add wave -noupdate -expand -group tsi24 -radix unsigned /tb_tx_top/uut/tsi24/data_out
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_data_wr_en
add wave -noupdate -expand -group tsi24 -radix unsigned /tb_tx_top/uut/tsi24/int_data_waddr
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_data_raddr
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_data_write_port_en
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_data_read_port_en
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_cfg_data
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_cfg_raddr
add wave -noupdate -expand -group tsi24 /tb_tx_top/uut/tsi24/int_cfg_port_en
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/clk
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/reset
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/data_valid
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/cfg
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/sel
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/cnt
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/raddr
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/int_sel
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/start
add wave -noupdate -expand -group ctl /tb_tx_top/uut/ctl_crossbar0/valid_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1325000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 278
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {119832 ps}
