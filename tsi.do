echo Compile vhd files
vcom src/tsi/* src/tb/tb_tsi.vhd

echo starting tb simulation
vsim work.tb_tsi


echo loading preconfigured memory
#do load_mem_linux.do
#do load_mem_windows.do
#mem load -i C:/Users/s123722/Desktop/vhdl/memorycfg/test.mem /tb_tsi/uut/dual_port_ram_rptr/ram
mem load -i /home/aj/development/ms/vhdl2/memorycfg/test.mem /tb_tsi/uut/dual_port_ram_rptr/ram





echo adding wave signals
do tb_tsi_wave.do


echo running for 6000 ns
run 1000 ns
wave zoom full
