onerror {resume}
quietly virtual signal -install /tb_flexe_top_prbs/tx { /tb_flexe_top_prbs/tx/data_in(1319 downto 1254)} B0
quietly virtual signal -install /tb_flexe_top_prbs/tx { /tb_flexe_top_prbs/tx/data_in(1253 downto 1188)} B1
quietly virtual signal -install /tb_flexe_top_prbs/tx { /tb_flexe_top_prbs/tx/data_in(65 downto 0)} B19
quietly virtual signal -install /tb_flexe_top_prbs/rx { /tb_flexe_top_prbs/rx/data_out(1319 downto 1254)} B0
quietly virtual signal -install /tb_flexe_top_prbs/rx { /tb_flexe_top_prbs/rx/data_out(65 downto 0)} B19
quietly virtual signal -install /tb_flexe_top_prbs/tx { /tb_flexe_top_prbs/tx/data_out(461 downto 396)} PHY2_LANE_3
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_flexe_top_prbs/clk
add wave -noupdate /tb_flexe_top_prbs/reset
add wave -noupdate /tb_flexe_top_prbs/data_valid
add wave -noupdate -expand -group tx -label ch_i -radix unsigned /tb_flexe_top_prbs/tx/data_in
add wave -noupdate -expand -group rx -label ch_o -radix unsigned /tb_flexe_top_prbs/rx/data_out
add wave -noupdate /tb_flexe_top_prbs/tx/int_new_cfg
add wave -noupdate /tb_flexe_top_prbs/rx/int_new_cfg
add wave -noupdate /tb_flexe_top_prbs/rx/int_cfg_stage1
add wave -noupdate /tb_flexe_top_prbs/rx/int_cfg_stage2
add wave -noupdate /tb_flexe_top_prbs/rx/int_cfg_stage3
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i0
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i1
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i2
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i3
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i4
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i5
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i6
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i7
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i8
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i9
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i10
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i11
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i12
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i13
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i14
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i15
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i16
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i17
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/i18
add wave -noupdate -group tx_cro -radix unsigned /tb_flexe_top_prbs/tx/crossbar/i19
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/sel
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o0
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o1
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o2
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o3
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o4
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o5
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o6
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o7
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o8
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o9
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o10
add wave -noupdate -group tx_cro -radix unsigned /tb_flexe_top_prbs/tx/crossbar/o11
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o12
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o13
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o14
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o15
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o16
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o17
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o18
add wave -noupdate -group tx_cro /tb_flexe_top_prbs/tx/crossbar/o19
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i0
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i1
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i2
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i3
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i4
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i5
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i6
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i7
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i8
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i9
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i10
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i11
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i12
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i13
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i14
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i15
add wave -noupdate -group rx_cro -radix unsigned /tb_flexe_top_prbs/rx/crossbar/i16
add wave -noupdate -group rx_cro -radix unsigned /tb_flexe_top_prbs/rx/crossbar/i17
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i18
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/i19
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/sel
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o0
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o1
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o2
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o3
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o4
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o5
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o6
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o7
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o8
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o9
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o10
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o11
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o12
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o13
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o14
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o15
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o16
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o17
add wave -noupdate -group rx_cro /tb_flexe_top_prbs/rx/crossbar/o18
add wave -noupdate -group rx_cro -radix unsigned /tb_flexe_top_prbs/rx/crossbar/o19
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/clk
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/reset
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/data_valid
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/cfg_data
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/cfg_waddr
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/cfg_wr_en
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/cfg
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/data_in
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/rdy
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/new_cfg
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/data_out
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_data_wr_en
add wave -noupdate -group rx_tsi0 -radix unsigned /tb_flexe_top_prbs/rx/tsi0/int_data_waddr
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_data_raddr
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_data_write_port_en
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_data_read_port_en
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_cfg_data
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_cfg_raddr
add wave -noupdate -group rx_tsi0 /tb_flexe_top_prbs/rx/tsi0/int_cfg_port_en
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/clk
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/reset
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/data_valid
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/cfg_data
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/cfg_waddr
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/cfg_wr_en
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/cfg
add wave -noupdate -group tx_tsi39 -radix unsigned /tb_flexe_top_prbs/tx/tsi39/data_in
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/rdy
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/new_cfg
add wave -noupdate -group tx_tsi39 -radix unsigned /tb_flexe_top_prbs/tx/tsi39/data_out
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/int_data_wr_en
add wave -noupdate -group tx_tsi39 -radix unsigned /tb_flexe_top_prbs/tx/tsi39/int_data_waddr
add wave -noupdate -group tx_tsi39 -radix unsigned /tb_flexe_top_prbs/tx/tsi39/int_data_raddr
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/int_data_write_port_en
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/int_data_read_port_en
add wave -noupdate -group tx_tsi39 -radix unsigned /tb_flexe_top_prbs/tx/tsi39/int_cfg_data
add wave -noupdate -group tx_tsi39 -radix unsigned /tb_flexe_top_prbs/tx/tsi39/int_cfg_raddr
add wave -noupdate -group tx_tsi39 /tb_flexe_top_prbs/tx/tsi39/int_cfg_port_en
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/clk
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/reset
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/data_valid
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/cfg_data
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/cfg_waddr
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/cfg_wr_en
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/cfg
add wave -noupdate -group tx_tsi0 -radix unsigned /tb_flexe_top_prbs/tx/tsi0/data_in
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/rdy
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/new_cfg
add wave -noupdate -group tx_tsi0 -radix unsigned /tb_flexe_top_prbs/tx/tsi0/data_out
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/int_data_wr_en
add wave -noupdate -group tx_tsi0 -radix unsigned /tb_flexe_top_prbs/tx/tsi0/int_data_waddr
add wave -noupdate -group tx_tsi0 -radix unsigned /tb_flexe_top_prbs/tx/tsi0/int_data_raddr
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/int_data_write_port_en
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/int_data_read_port_en
add wave -noupdate -group tx_tsi0 -radix unsigned /tb_flexe_top_prbs/tx/tsi0/int_cfg_data
add wave -noupdate -group tx_tsi0 -radix unsigned /tb_flexe_top_prbs/tx/tsi0/int_cfg_raddr
add wave -noupdate -group tx_tsi0 /tb_flexe_top_prbs/tx/tsi0/int_cfg_port_en
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/clk
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/reset
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/data_valid
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/cfg_data
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/cfg_waddr
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/cfg_wr_en
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/cfg
add wave -noupdate -group tx_tsi19 -radix unsigned /tb_flexe_top_prbs/tx/tsi19/data_in
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/rdy
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/new_cfg
add wave -noupdate -group tx_tsi19 -radix unsigned /tb_flexe_top_prbs/tx/tsi19/data_out
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/int_data_wr_en
add wave -noupdate -group tx_tsi19 -radix unsigned /tb_flexe_top_prbs/tx/tsi19/int_data_waddr
add wave -noupdate -group tx_tsi19 -radix unsigned /tb_flexe_top_prbs/tx/tsi19/int_data_raddr
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/int_data_write_port_en
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/int_data_read_port_en
add wave -noupdate -group tx_tsi19 -radix unsigned /tb_flexe_top_prbs/tx/tsi19/int_cfg_data
add wave -noupdate -group tx_tsi19 -radix unsigned /tb_flexe_top_prbs/tx/tsi19/int_cfg_raddr
add wave -noupdate -group tx_tsi19 /tb_flexe_top_prbs/tx/tsi19/int_cfg_port_en
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/clk
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/reset
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/data_valid
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/cfg_data
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/cfg_waddr
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/cfg_wr_en
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/cfg
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/data_in
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/rdy
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/new_cfg
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/data_out
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_data_wr_en
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_data_waddr
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_data_raddr
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_data_write_port_en
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_data_read_port_en
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_cfg_data
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_cfg_raddr
add wave -noupdate -group rx_tsi17 /tb_flexe_top_prbs/rx/tsi17/int_cfg_port_en
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/clk
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/reset
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/data_valid
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/cfg_data
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/cfg_waddr
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/cfg_wr_en
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/cfg
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/data_in
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/rdy
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/new_cfg
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/data_out
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_data_wr_en
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_data_waddr
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_data_raddr
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_data_write_port_en
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_data_read_port_en
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_cfg_data
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_cfg_raddr
add wave -noupdate -expand -group tx_tsi31 /tb_flexe_top_prbs/tx/tsi31/int_cfg_port_en
add wave -noupdate /tb_flexe_top_prbs/data_flex_e
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {805000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 273
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {10855 ps}
